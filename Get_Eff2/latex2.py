import os
import re

picture_dir = './'

# List all files matching "EFF2c_*png"
eff2c_files = [f for f in os.listdir(picture_dir) if f.startswith("EFF2c_") and f.endswith(".png") and "prompt" in f and "med" in f] # medium
#eff2c_files = [f for f in os.listdir(picture_dir) if f.startswith("EFF2c_") and f.endswith(".png") and "prompt" in f] # Reco 
eff2c_files.sort()
eff2_files = [f.replace("EFF2c_", "EFF2_") for f in eff2c_files]

labels = ["0p0001ns","0p001ns","0p01ns","0p1ns","1ns","10ns"]

# Find a file containing "Ztautau"
ztautau_file = [f for f in os.listdir(picture_dir) if "Ztautau" in f and f.endswith(".png")]

# Define LaTeX template
latex_template = r'''
\documentclass{article}
\usepackage{graphicx}
\usepackage{geometry}

\geometry{margin=0.2in}

\begin{document}
%\section*{Pictures}
'''
def getName(input_string):
    pattern = r'.*_(\d+)_(.*)_LeadingTrk_d0_Lxy_([^_]+)_(1p|1p_med).png'
    match = re.match(pattern, input_string)
    return "stau"+match.group(1)+"-n1"+match.group(2)+"-taufrom"+match.group(3)+"-"+match.group(4).replace("_","-")

# Function to add a row of pictures to the LaTeX file
def add_picture_row(latex_file, pictures):
    latex_file.write('\\begin{figure}[htbp]\n')
    name = getName(pictures[1])
    latex_file.write('\\textbf{'+name+'}\\par\\medskip\n')
    for picture in pictures:
        latex_file.write(f'    \\includegraphics[width=0.3\\textwidth]{{{picture}}}\n')
    latex_file.write('\\end{figure}\n\n')

# Initialize LaTeX file
with open('output2.tex', 'w') as latex_file:
    latex_file.write(latex_template)
    
    i=0
    for p1, p2 in zip(eff2c_files, eff2_files):
        if "med" in p1:
            zfile = list(filter(lambda x: "med" in x, ztautau_file))[0]
        else:
            zfile = list(filter(lambda x: "med" not in x, ztautau_file))[0]
        row = [zfile, p2, p1]
        add_picture_row(latex_file, row)
        for label in labels:
            row = [zfile, p2.replace("prompt", label), p1.replace("prompt", label) ]
            add_picture_row(latex_file, row)
        latex_file.write('\n\clearpage')
        i+=1
        #if i>40: break
    latex_file.write('\n\end{document}')

# Compile the LaTeX file to generate the PDF
os.system('pdflatex output2.tex')

# Clean up intermediate files
os.system('rm output2.aux output2.log output2.tex')
