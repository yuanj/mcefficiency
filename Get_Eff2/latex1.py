import os
import re

# Directory containing the pictures
picture_dir = './'

# List all files matching "EFF2c_*png"
eff2c_files = [f for f in os.listdir(picture_dir) if f.startswith("EFF2c_") and f.endswith(".png") and "prompt" in f]
eff2c_files.sort()

# Replace "EFF2c" with "EFF2" in the filenames
eff2_files = [f.replace("EFF2c_", "EFF2_") for f in eff2c_files]

# Find a file containing "Ztautau"
ztautau_file = [f for f in os.listdir(picture_dir) if "Ztautau" in f and f.endswith(".png")]

# Define LaTeX template
latex_template = r'''
\documentclass{article}
\usepackage{graphicx}
\usepackage{geometry}

\geometry{margin=0.5in}

\begin{document}
\section*{Pictures}
'''
def getName(input_string):
    pattern = r'.*_(\d+)_([\d.]+ns|prompt)_LeadingTrk_d0_Lxy_([^_]+)_([\d.]+p).*'
    match = re.match(pattern, input_string)
    return "stau"+match.group(1)+"-n1"+match.group(2)+"-taufrom"+match.group(3)+"-"+match.group(4)

# Function to add a row of pictures to the LaTeX file
def add_picture_row(latex_file, pictures):
    latex_file.write('\\begin{figure}[htbp]\n')
    name = getName(pictures[1])
    latex_file.write('\\textbf{'+name+'}\\par\\medskip\n')
    for picture in pictures:
        latex_file.write(f'    \\includegraphics[width=0.3\\textwidth]{{{picture}}}\n')
    latex_file.write('\\end{figure}\n\n')

# Initialize LaTeX file
with open('output1.tex', 'w') as latex_file:
    latex_file.write(latex_template)

    for p1, p2 in zip(eff2c_files, eff2_files):
        row = [ztautau_file[0], p2, p1]
        add_picture_row(latex_file, row)
    
    latex_file.write('\n\end{document}')

# Compile the LaTeX file to generate the PDF
os.system('pdflatex output1.tex')

# Clean up intermediate files
os.system('rm output1.aux output1.log output1.tex')

