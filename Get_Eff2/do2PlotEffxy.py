import json
import math
import numpy as np
import ROOT
import pickle
from array import array
from multiprocessing import Process
import re
# draw nom or denomwith rawnumber to check statistics
# draw efficiency plot
# create efficiency workspace

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+6, 
        ROOT.kYellow, ROOT.kRed
]
ROOT.gROOT.SetBatch(True)


def tobinning(x):
    return [len(x)-1,x]

def divide_eff(histup, histdown):
    histeff = histup.Clone()
    for i in range(histup.GetNbinsX()):
        for j in range(histup.GetNbinsY()):
            k = histup.GetBinContent(i+1,j+1)
            n = histdown.GetBinContent(i+1,j+1)
            if n==0:
                eff = 0
            else:
                eff = k/n
            Veff = (k+1)*(k+2)/((n+2)*(n+3)) - (k+1)**2/(n+2)**2
            histeff.SetBinContent(i+1,j+1,eff)
            histeff.SetBinError(i+1,j+1,math.sqrt(Veff))
    return histeff

def readhist(filelist, xbinning, ybinning):
    xlist, ylist, effList, wlist = [],[],[],[]
    for filename in filelist:
        with open(filename,"rb") as file:
            datadict = pickle.load(file)
            xlist.extend(datadict["xval"])
            ylist.extend(datadict["yval"])
            effList.extend(datadict["eff"])
            wlist.extend(datadict["wei"])
        process = filename
    process = '_'.join( process.split('_')[2:5] )
    
    arg_h1 = [process, process]+xbinning+ybinning
    arg_h2 = [process+"_down", process+"_down"]+xbinning+ybinning
    hist_up = ROOT.TH2F(*arg_h1)
    hist_down = ROOT.TH2F(*arg_h2)
    
    for x,y,e,w in zip(xlist, ylist, effList,wlist):
        if e>0: 
            hist_up.Fill(x,y)
        hist_down.Fill(x,y)
    hist = divide_eff(hist_up, hist_down)
    return hist, hist_up, hist_down

def compEff(sigjson, smjson, smname, xbins, ybins):
    # check statistics
    xbinning = tobinning(xbins)
    ybinning = tobinning(ybins)

    matched = re.match("effxy_TruthTau_leading_(.*)_TruthTau_leading_(.*)_(.p.*).json",sigjson)
    xtitle, ytitle, tag = matched.groups()
    with open(sigjson) as f:
        data = json.load(f)
        sortedName = sorted(list(data.keys()))
        with open(smjson) as sj:
            smdata = json.load(sj)
            filelist = smdata[smname]
            hist_ztt,_,__ = readhist(filelist, xbinning, ybinning)
        for i, process in enumerate(sortedName):
            hist ,_,__= readhist(data[process], xbinning, ybinning)
            arg_hn = [process+"new", process+"new"] + xbinning + ybinning
            histn = ROOT.TH2F(*arg_hn)
            arg_hn1 = [process+"new1", process+"new1"] + xbinning + ybinning
            histn1 = ROOT.TH2F(*arg_hn1)
            arg_hn2 = [process+"new2", process+"new2"] + xbinning + ybinning
            histn2 = ROOT.TH2F(*arg_hn2)
            for i in range(len(xbins)-1):
                for j in range(len(ybins)-1):
                    if hist.GetBinContent(i+1,j+1)>0 and hist_ztt.GetBinContent(i+1,j+1)>0:
                        histn.SetBinContent(i+1,j+1, abs(hist.GetBinContent(i+1,j+1) - hist_ztt.GetBinContent(i+1,j+1))/math.sqrt(hist.GetBinError(i+1,j+1)**2 + hist_ztt.GetBinError(i+1,j+1)**2) )
                    else:
                        histn.SetBinContent(i+1,j+1,0)
                    histn1.SetBinContent(i+1,j+1, abs(hist.GetBinContent(i+1,j+1) - hist_ztt.GetBinContent(i+1,j+1)) )
                    histn2.SetBinContent(i+1,j+1, math.sqrt(hist.GetBinError(i+1,j+1)**2 + hist_ztt.GetBinError(i+1,j+1)**2) )

            canvas = ROOT.TCanvas("", "", 1600, 1200)
            histn.SetBarOffset(0.2)
            ROOT.gStyle.SetPaintTextFormat("4.3f");
            histn.Draw("COLZ TEXT")
            histn.SetStats(0)

            histn1.SetMarkerColor(ROOT.kGreen)
            histn1.Draw("TEXT SAME")

            histn2.SetBarOffset(-0.2)
            histn2.SetMarkerColor(ROOT.kRed)
            histn2.Draw("TEXT SAME")

            histn.SetXTitle(xtitle)
            histn.SetYTitle(ytitle)
            histn.GetZaxis().SetRangeUser(0, 4);
            canvas.SaveAs("EFF2c_"+process+"_"+xtitle+"_"+ytitle+"_"+tag+".png")

def drawEff(jfile, xbins, ybins, saveROOT=False):
    # check statistics
    xbinning = tobinning(xbins)
    ybinning = tobinning(ybins)

    matched = re.match("effxy_TruthTau_leading_(.*)_TruthTau_leading_(.*)_(.p.*).json",jfile)
    xtitle, ytitle, tag = matched.groups()
    if saveROOT: 
        root_file = ROOT.TFile(jfile.split('.')[0]+".root", "RECREATE")
    with open(jfile) as f:
        data = json.load(f)
        sortedName = sorted(list(data.keys()))

        for i, process in enumerate(sortedName):
            hist, hist_num, hist_denum = readhist(data[process], xbinning, ybinning)

            if saveROOT:
                hist.Write()
            else:
                canvas = ROOT.TCanvas("", "", 1600, 1200)
                hist.SetBarOffset(0.2)
                hist.SetStats(0)
                hist.Draw("COLZ TEXT")
                hist.SetXTitle(xtitle)
                hist.SetYTitle(ytitle)

                arg_hn = [process+"new",process+"new"]+xbinning+ybinning
                histn = ROOT.TH2F(*arg_hn)
                for i in range(hist.GetNbinsX()):
                    for j in range(hist.GetNbinsY()):
                        if hist.GetBinContent(i+1,j+1)>0:
                            histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1))
                            #histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1)/hist.GetBinContent(i+1,j+1))
                        else:
                            histn.SetBinContent(i+1,j+1,0)
                        #histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1))
                histn.SetMarkerColor(ROOT.kRed)
                histn.SetBarOffset(-0.1)
                histn.SetStats(0)
                ROOT.gStyle.SetPaintTextFormat("4.3f");
                histn.Draw("TEXT SAME")
                hist.Draw("TEXT SAME")

                hist_num.SetMarkerColor(ROOT.kBlue)
                hist_denum.SetMarkerColor(ROOT.kBlue)
                hist_num.SetBarOffset(-0.25)
                hist_denum.SetBarOffset(-0.4)
                hist_num.Draw("TEXT SAME")
                hist_denum.Draw("TEXT SAME")
                hist.GetZaxis().SetRangeUser(0., 1.);

                latex = ROOT.TLatex()
                latex.SetNDC ()
                latex.SetTextSize (0.06)
                latex.DrawText (0.7 ,0.95 ,tag[0:2])
                if tag[-1] == "f" or tag[-1] == "c":
                    latex.SetTextSize (0.06)
                    latex.DrawText (0.76 ,0.95 , "forward" if tag[-1] == "f" else "central")

                canvas.SaveAs("EFF2_"+process+"_"+xtitle+"_"+ytitle+"_"+tag+".png")

    if saveROOT: 
        root_file.Close()
#lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
#lin1 = array('f', [-3, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0])

#din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
#din2 = array('f', [-6,]+[x/2. for x in range(-3,3,1)]+[4,])
#din3 = array('f', [x/2. for x in range(-3,3,1)])

#ptbin = array('f', [40,60,80,100,120,160,200])
#ein1 = array('f', [x/10. for x in range(-30,31,5)])
#ein2 = array('f', [-3,-1.52,-1.37,1.37,1.52,3])

#din2 = array('f', [-6,-0.5,0,0.5,1,2,4])
#lin1 = array('f', [-3,1,1.5,2,2.5,3,4])
#lin1 = array('f', [-3,0,1,1.5,2,2.5,3,4]) # consider 0

din1 = array('f', [x for x in range(-6,5,1)])
lin1 = array('f', [x for x in range(-3,5,1)])

arglist = [
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p_med.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_3p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_3p_med.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p_med.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_3p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_3p_med.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_1p_med.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_3p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_3p_med.json', din1, lin1],

]

processLst = []
for pargs in arglist:
    #processLst.append(Process(target=drawEff, args=(pargs)))
    processLst.append(Process(target=drawEff, args=(pargs+[True,])))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()

#arglist1 = [
#    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p.json','Ztautau', din1, lin1],
#    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p_med.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p_med.json','Ztautau', din1, lin1],
#    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_3p.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_3p.json','Ztautau', din1, lin1],
#    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_1p.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p.json','Ztautau', din1, lin1],
#    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_1p_med.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p_med.json','Ztautau', din1, lin1],
#    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_3p.json','effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_3p.json','Ztautau', din1, lin1],
#]
#processLst1 = []
#for pargs in arglist1:
#    processLst1.append(Process(target=compEff, args=pargs))
#    processLst1[-1].start()
#for i, proc in enumerate(processLst1):
#    proc.join()
