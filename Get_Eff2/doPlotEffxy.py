import time
import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process, Queue
import pickle
from array import array
import json

def find_max_eff_number(directory):
    max_number = -1
    for filename in os.listdir(directory):
        full_path = os.path.join(directory, filename)
        if re.match(r'effxy_sub\.sh\.\d+', filename):
            number = int(re.search(r'\d+$', filename).group())
            max_number = max(max_number, number)
    return max_number

def run(func, argList):
    out = []
    for args in argList:
        out.append(func(*args))
    return out

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return "gmsb_"+re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Stau_" in fullName:
        matched = re.match(r".*Stau_(.*0)_50_.33_(.*ns).*", folder)
        return "stau_"+matched.groups()[0]+"_"+matched.groups()[1]
    elif "RPCRPV" in fullName:
        matched = re.match(r".*StauRPCRPV_(.*0)_50_.33.*", folder)
        return "stau_"+matched.groups()[0]+"_prompt"
    elif "N1N2" in fullName:
        matched = re.match(r".*N1N2_.*_(.*)_(.*)_(.*ns).*", folder) 
        return "N1N2_"+matched.groups()[0]+"_"+matched.groups()[1]+"_"+matched.groups()[2]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName:
        return re.findall(r".*Py8_(ttbar_.*)_.*", folder)[0]
    else:
        raise Exception("Sorry, the process isn't matched.")

def getName(num, procName, xbranch, ybranch, match_branch, ptcut, ptcut2, nprong, cut_branch, clow, chigh, tag):
    return "effxy_"+str(num)+"_"+procName+"_"+xbranch+"_"+ybranch+"_tau"+str(int(nprong))+"p"+str(int(ptcut/1e3))+str(int(ptcut2/1e3))+"_"+cut_branch+"_"+str(float(clow))+"_"+str(float(chigh))+"_"+tag+".pk"

def leadingplotEff(pattern, xbranch, ybranch, match_branch, nickName, ptcut=40e3, ptcut2=1e6, nprong=-1, cut_branch='', clow='', chigh=''):
    treename = 'analysis'
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2'
    outputs = defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        procName = getProc(root)
        if "_allhad_" in procName or "_dil_" in procName: continue
        r_files = filter(lambda x: "root" in x, files)
        for file in r_files:
            num = str(find_max_eff_number('.')+1)
            with open('effxy_sub.sh.'+num,'w') as f:
                f.write('python real_job_effxy_leading.py '+' '.join([num, root, file, xbranch, ybranch, match_branch, str(ptcut), str(ptcut2), str(nprong), cut_branch, str(clow), str(chigh), nickName]))
            os.system('chmod u+x effxy_sub.sh.'+str(num))
            output = getName(num, procName,xbranch,ybranch, match_branch, ptcut, ptcut2, nprong, cut_branch, clow, chigh, nickName) 
            outputs[procName].append(output)

    with open('effxy_'+xbranch+'_'+ybranch+'_'+nickName+'.json', "w") as outfile:
        json.dump(outputs, outfile)
    return 'effxy_'+xbranch+"_"+ybranch+'_'+nickName+'.json'

if __name__ == '__main__':
    os.system('rm -f effxy_sub.sh.*')
    argListLeading = [
        #(".*n09_E.*N1N2_.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'nn1_1p',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
        #(".*n09_E.*N1N2_.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'nn1_3p',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),

        #(".*n09_E.*N1N2_.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'nn2_1p',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 2, 4),
        #(".*n09_E.*N1N2_.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'nn2_3p',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 2, 4),

        # w/o OLR
        #                (".*n11_E.*Stau(RPCRPV_|_).00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'n1_1p_bs',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
        #                (".*n11_E.*Stau(RPCRPV_|_).00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'n1_3p_bs',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),

        #                (".*n11_E.*Stau(RPCRPV_|_).00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'stau_1p_bs',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),
        #                (".*n11_E.*Stau(RPCRPV_|_).00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'stau_3p_bs',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),

        #(".*n11_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'sm_1p_bs',  40e3, 1e6, 1, 'TruthTau_leading_Eta', 0, 2.5),
        #(".*n11_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'sm_3p_bs',  40e3, 1e6, 3, 'TruthTau_leading_Eta', 0, 2.5),

        # w/ OLR
        (".*n14_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'sm_1p',  40e3, 1e6, 1, 'TruthTau_leading_Eta', 0, 2.5),
        (".*n14_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'sm_3p',  40e3, 1e6, 3, 'TruthTau_leading_Eta', 0, 2.5),

                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'n1_1p',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'n1_3p',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),

                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'stau_1p',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),
                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", 'stau_3p',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),

        (".*n14_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'sm_1p_med',  40e3, 1e6, 1, 'TruthTau_leading_Eta', 0, 2.5),
        (".*n14_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'sm_3p_med',  40e3, 1e6, 3, 'TruthTau_leading_Eta', 0, 2.5),

                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'n1_1p_med',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'n1_3p_med',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),

                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'stau_1p_med',  40e3, 1e6, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),
                        (".*n14_E.*Stau(RPCRPV_|_).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMedium", 'stau_3p_med',  40e3, 1e6, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 1, 3),

        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '1pssA',  40e3, 60e3, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '3pssA',  40e3, 60e3, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '1pssB',  60e3, 80e3, 1, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '3pssB',  60e3, 80e3, 3, 'TruthTau_leading_LastSUSY_Mother_absPDGID', 0, 2),

        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '1psmA',  40e3, 60e3, 1, 'TruthTau_leading_Eta', 0, 2.5),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '3psmA',  40e3, 60e3, 3, 'TruthTau_leading_Eta', 0, 2.5), #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '1psmB',  60e3, 80e3, 1, 'TruthTau_leading_Eta', 0, 2.5),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", '3psmB',  60e3, 80e3, 3, 'TruthTau_leading_Eta', 0, 2.5),

        # eta
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '1pssc', 40e3, 1, 'TruthTau_leading_Eta', 0, 1.37),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '1psmc', 40e3, 1, 'TruthTau_leading_Eta', 0, 1.37),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '1pssf', 40e3, 1, 'TruthTau_leading_Eta', 1.52, 2.5),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '1psmf', 40e3, 1, 'TruthTau_leading_Eta', 1.52, 2.5),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '3pssc', 40e3, 3, 'TruthTau_leading_Eta', 0, 1.37),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '3psmc', 40e3, 3, 'TruthTau_leading_Eta', 0, 1.37),
        #                (".*n09_E.*Stau_.00.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '3pssf', 40e3, 3, 'TruthTau_leading_Eta', 1.52, 2.5),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_Pt", "TruthTau_leading_IsMatched", '3psmf', 40e3, 3, 'TruthTau_leading_Eta', 1.52, 2.5),
    ]
    out = run(leadingplotEff, argListLeading)
    print ",".join(out)
    num = find_max_eff_number('.')+1
    print num
    os.system('hep_sub effxy_sub.sh."%{ProcId}" -g atlas -n '+str(num))
