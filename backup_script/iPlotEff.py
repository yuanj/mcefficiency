import time
import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process, Queue
import pickle
from array import array

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+7, 
        ROOT.kMagenta, 
        ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]
CONCURRENCY = 30

def holdon(pop, wait=5):
    """
    Args:
        pop: a list of Popen
        concurrency: max number of running processes
        wait: time interval between check of processes
    """
    while True:
        poll = sum([1 if p.exitcode is None else 0 for p in pop])  # 1(None) for running, 0(0,1) for finished
        if poll < CONCURRENCY:
            break
        time.sleep(wait)

def submit(PlotHist, argLst):
    start = time.time()
    processLst=[]
    queue = Queue()
    def wrapper(input, q1):
        a = PlotHist(*input)
        q1.put(a)
    for argTuple in argLst:
        processLst.append(Process(target=wrapper, args=(argTuple,queue)))
        processLst[-1].start()
        print "Process Start:",len(processLst),"/",len(argLst)
        holdon(processLst)
    for i, proc in enumerate(processLst):
        proc.join()
        end = time.time()
        print "Process Done:",i+1,"/",len(processLst)
    queue.put('done')
    outputs=[]
    #item=""
    while True:
        item=queue.get()
        if item== "done":
            break
        outputs.append(item)
    return outputs

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return "gmsb_"+re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Stau_" in fullName:
        return "lle_"+re.findall(r".*Stau_.00_50_.33_(.*ns).*", folder)[0]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName and ("ttbar_allhad" not in fullName):
        #return re.findall(r".*Py8_(ttbar_.*)_ANA.*", folder)[0]
        return re.findall(r".*Py8_(.*)_.*_ANA.*", folder)[0]
    else:
        return "ttbar_allhad"

def flat_filter(truth_branch, match_branch, Weight, passcut):
    xArray, effArray, weiArray = [], [], []
    for val, ismatched, ispassed in zip(truth_branch, match_branch, passcut):
        if ispassed:
            xArray.append(val)
            effArray.append(ismatched)
            weiArray.append(Weight)
    return xArray, effArray, weiArray

def calc_eff(root, ifile, truth_branch, match_branch, ptcut, cut_branch, cfun, clow, chigh):
    outname = "ieffcache_"+truth_branch+"_pt"+str(ptcut/1e3)+"_"+"".join(cut_branch)+str(clow)+str(chigh)+"_"+ifile+".pk"
    procName = getProc(root)
    if os.path.isfile(outname):
        return procName, outname
    file = ROOT.TFile.Open(os.path.join(root, ifile))
    start = time.time()
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:
        ptpass = [ipt>ptcut for ipt in getattr(event, 'TruthTau_Vis_Pt')]
        if cut_branch!="":
            passcut = [(cfun(val)>clow and cfun(val)<chigh) for val in getattr(event, cut_branch)]
            passcut = [(p and c) for p, c in zip(ptpass, passcut)]
        else:
            passcut = ptpass

        ixArray, ieffArray, iweiArray = flat_filter(list(getattr(event, truth_branch)), list(getattr(event, match_branch)), getattr(event,'Weight'), passcut)

        xArray.extend(ixArray)
        effArray.extend(ieffArray)
        weiArray.extend(iweiArray)
        if len(weiArray)>1e5: break
    end = time.time()
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname
    #return procName, xArray, effArray, weiArray

def plotEff(pattern, truth_branch, match_branch, nick_name, binning, ptcut=40e3, cut_branch="", cfun="", clow=0, chigh=1e3):
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2'
    treename = 'analysis'
    xfunc = lambda x: math.log(abs(x)+1e-10, 10)
    if "TruthTau_Vis_Pt" in truth_branch:
        xfunc = lambda x : x/1e3
    if "TruthTau_Vis_Eta" in truth_branch:
        xfunc = lambda x :abs(x)

    processes = []
    submit_list = []
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        if "StauStau_directLLP" in root: continue
        procName = getProc(root)
        processes.append(procName)
        r_files = filter(lambda x: "root" in x, files)
        for i, ifile in enumerate(r_files):
            if i>20: break
            submit_list.append((root, ifile, truth_branch, match_branch, ptcut, cut_branch, cfun, clow, chigh))
    files = submit(calc_eff, submit_list)
    sortedName = sorted(list(set(processes)));
    canvas = ROOT.TCanvas(truth_branch+"canvas", truth_branch+"Canvas Title", 1600, 1200)
    isFirst = True
    legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)

    tmp_store=[]
    for i, process in enumerate(sortedName):
        print "Draw",process,'.',
        xbranchList, xeffList, xweiList = [],[],[]
        for procName, filename in files:
            if procName==process:
                with open(filename,"rb") as file:
                    datadict = pickle.load(file)
                    xbranchList.extend(datadict["xval"])
                    xeffList.extend(datadict["eff"])
                    xweiList.extend(datadict["wei"])
        print "There are ", len(xbranchList), " raw number for ", process
        #xbranchList, xeffList, xweiList = xvarDict[process], effvarDict[process], weiDict[process]
        sumwei = np.array(xweiList).sum()
        xweiList = xweiList / sumwei
        arg_h1 = [process+truth_branch, process+truth_branch] + binning
        arg_down=[process+truth_branch+"down", process+truth_branch+"down"] + binning
        hist = ROOT.TH1F(*arg_h1)
        hist_down = ROOT.TH1F(*arg_down)
        for itau in range(len(xbranchList)):
            hist.Fill(xfunc(xbranchList[itau]), xweiList[itau]*xeffList[itau])
            hist_down.Fill(xfunc(xbranchList[itau]), xweiList[itau])
        hist.Divide(hist_down)
        hist.SetLineColor(UserColors[i])
        hist.SetLineWidth(3)
        if isFirst:
            hist.Draw()
            hist.SetXTitle(truth_branch)
            hist.SetYTitle("efficiency")
            hist.SetTitle(truth_branch)
            hist.GetYaxis().SetRangeUser(0., 1.);
            isFirst = False
        else:
            hist.Draw('SAME')
        hist.SetStats(0)
        legend.AddEntry(hist, process, "l")
        tmp_store.append(hist)
    legend.Draw()
    canvas.SaveAs("EFF1_"+nick_name+truth_branch+".png")
    canvas.SaveAs("EFF1_"+nick_name+truth_branch+".pdf")
    return

#import argparse
#parser = argparse.ArgumentParser(description='Process parameters.')
#parser.add_argument('--submission', action='store_true', default=False, help='submit jobs with many settings')
#parser.add_argument('--run', action='store_true', default=False, help='submit jobs with many settings')
#parser.add_argument('target', help='input parameters')
#parser.add_argument('tag', help='input parameters')
#parser.add_argument('bins', help='input parameters')
#parser.add_argument('xmin', help='input parameters')
#parser.add_argument('xmax', help='input parameters')
#parser.add_argument('cut', help='input parameters')
#parser.add_argument('cmin', help='input parameters')
#parser.add_argument('cmax', help='input parameters')
#args = parser.parse_args()



pattern1 = ".*Testn03.*"#StauStau_.*"
patterns = ".*Testn03.*Stau_.*"#StauStau_.*"
patternz = ".*Testn03.*Ztautau.*"#StauStau_.*"
patternt = ".*Testn03.*ttbar.*"#StauStau_.*"
#pattern2 = ".*Test_v03.*"
#lin2 = array('f', [-3,-1]+[x/2. for x in range(-1,9,1)])
#lin3 = array('f', [-3,-1.5]+[x/2. for x in range(-2,9,1)])
#lin4 = array('f', [-3,-2]+[x/2. for x in range(-3,9,1)])
#lin5 = array('f', [-3,-2.5]+[x/2. for x in range(-4,9,1)])
#din2 = array('f', [-6,-3.5]+[x/2. for x in range(-6,9,1)])
#din3 = array('f', [-6,-4]+[x/2. for x in range(-7,9,1)])
#din4 = array('f', [-6,-4.5]+[x/2. for x in range(-8,9,1)])
#din5 = array('f', [-6,-5]+[x/2. for x in range(-9,9,1)])
#din6 = array('f', [-6,-5.5]+[x/2. for x in range(-10,9,1)])
lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])

dinv0 = array('f', [-6,-1.5,-1,0,4])
dinv1 = array('f', [-6,-1.5,-1,-0.5,0,0.5,1,4])
dinv2 = array('f', [-6,-1.5,-1,-0.5,0,1,4])
dinv3 = array('f', [-6,-0.5,0,1,2,4])
dinv4 = array('f', [-6,0,2,2.5,4])

argLst=[
    # the very start point
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n0", [10, -1, 4], 40e3),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n0", [12, -4, 2], 40e3),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n1", [14, -3, 4], 40e3),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n1", [20, -6, 4], 40e3),
    
    # 1-d efficiency with good binning.
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n2", [len(lin1)-1, lin1 ], 40e3),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2", [len(din1)-1, din1], 40e3),
    #(patternz, "TruthTau_Lxy", "TruthTau_IsMatched", "n2z", [len(lin1)-1, lin1 ], 40e3),
    #(patternz, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2z", [len(din1)-1, din1], 40e3),
    #(patternt, "TruthTau_Lxy", "TruthTau_IsMatched", "n2t", [len(lin1)-1, lin1 ], 40e3),
    #(patternt, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2t", [len(din1)-1, din1], 40e3),

    # efficiency dependence on pt 
    #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n1", [16, 40, 200], 40e3),
    #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n2", [16, 40, 200], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -10,-0.5),
    #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n3", [16, 40, 200], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -0.5),

    # 2d check for d0 and Lxy
    # separate displaced and prompt signal tau by d0 
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v0", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -6,-3),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v1", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -3,-2),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v2", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -2,-1.5),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v3", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -1.5,-1),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v4", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -1,-0.5),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v5", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -0.5,0),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v6", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 0,0.5),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v7", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 0.5,1),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v8", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 1,1.5),
    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v9", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 1.5,4),

    # 2d check for d0 and Lxy
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v0", [len(dinv0)-1, dinv0], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), -3,-0.5),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v1", [len(dinv0)-1, dinv0 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), -0.5,0),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v2", [len(dinv1)-1, dinv1 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 0,0.5),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v3", [len(dinv1)-1, dinv1 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 0.5,1),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v4", [len(dinv2)-1, dinv2 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 1,1.5),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v5", [len(dinv3)-1, dinv3 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 1.5,2),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v6", [len(dinv4)-1, dinv4 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 2,2.5),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v7", [1, -6, 4], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 2.5,4),

    # check eta 
    #(pattern1, "TruthTau_Vis_Eta", "TruthTau_IsMatched", "n1", [13,0,2.6], 40e3),

    # check efficiency separately
    #(".*Testn07.*", "TruthTau_Lxy", "TruthTau_IsMatched", "s1", [len(lin1)-1, lin1 ], 40e3, "TruthTau_Mother_PDGID", lambda x : abs(x), 0, 1e6),
    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "s2", [len(din1)-1, din1], 40e3, "TruthTau_Mother_PDGID", lambda x : abs(x), 1e6, 1e7),
]
submit(plotEff, argLst)
