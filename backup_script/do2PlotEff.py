import json
import numpy as np
import math
import ROOT
import pickle
from array import array
from multiprocessing import Process
import os

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+7, 
        ROOT.kMagenta, 
        ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]*20

lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
dinv0 = array('f', [-6,-1.5,-1,0,4])
dinv1 = array('f', [-6,-1.5,-1,-0.5,0,0.5,1,4])
dinv2 = array('f', [-6,-1.5,-1,-0.5,0,1,4])
dinv3 = array('f', [-6,-0.5,0,1,2,4])
dinv4 = array('f', [-6,0,2,2.5,4])

ptbin = array('f', list(range(40,210,10)))
etabin= array('f', [x/10. for x in range(0,29,2)])
etabin1= array('f', [0,3.0])
# for 1 figure, pass 1 json file from last step, and binning 

def tobinning(x):
    return [len(x)-1,x]

def draw(file, bins):
    binning = tobinning(bins)
    with open(file) as f:
        data = json.load(f)

        truth_branch = file.split('.')[1].rsplit('_',1)[0]
        nick_name = file.split('.')[1].rsplit('_',1)[1]

        sortedName = sorted(list(data.keys()))
        canvas = ROOT.TCanvas(truth_branch+"canvas", truth_branch+"Canvas Title", 1600, 1200)
        isFirst = True
        legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)

        tmp_store=[]
        for i, process in enumerate(sortedName):
            print "Draw",process,'.',
            xbranchList, xeffList, xweiList = [],[],[]
            for filename in data[process]:
                with open(filename,"rb") as file:
                    datadict = pickle.load(file)
                    xbranchList.extend(datadict["xval"])
                    xeffList.extend(datadict["eff"])
                    xweiList.extend(datadict["wei"])

            print "There are ", len(xbranchList), " raw number for ", process
            #xbranchList, xeffList, xweiList = xvarDict[process], effvarDict[process], weiDict[process]
            sumwei = np.array(xweiList).sum()
            xweiList = xweiList / sumwei
            arg_h1 = [process+truth_branch, process+truth_branch] + binning
            arg_down=[process+truth_branch+"down", process+truth_branch+"down"] + binning
            hist = ROOT.TH1F(*arg_h1)
            hist_down = ROOT.TH1F(*arg_down)

            xfunc = lambda x: math.log(abs(x)+1e-10, 10)
            if "TruthTau_Vis_Pt" in truth_branch:
                xfunc = lambda x : x/1e3
            if "TruthTau_Vis_Eta" in truth_branch:
                xfunc = lambda x :abs(x)

            for itau in range(len(xbranchList)):
                hist.Fill(xfunc(xbranchList[itau]), xweiList[itau]*xeffList[itau])
                hist_down.Fill(xfunc(xbranchList[itau]), xweiList[itau])
            hist.Divide(hist_down)
            hist.SetLineColor(UserColors[i])
            hist.SetLineWidth(3)
            if isFirst:
                hist.Draw()
                hist.SetXTitle(truth_branch)
                hist.SetYTitle("efficiency")
                hist.SetTitle(truth_branch)
                hist.GetYaxis().SetRangeUser(0., 1.);
                isFirst = False
            else:
                hist.Draw('SAME')
            hist.SetStats(0)
            legend.AddEntry(hist, process, "l")
            tmp_store.append(hist)
        legend.Draw()
        canvas.SaveAs("EFF1_"+nick_name+truth_branch+".png")
        canvas.SaveAs("EFF1_"+nick_name+truth_branch+".pdf")

arglist = [
    # ['eff.TruthTau_LeadingTrk_d0_ssm.json', din1],
    # ['eff.TruthTau_LeadingTrk_d0_st1.json', din1],  
    # ['eff.TruthTau_LeadingTrk_d0_st2.json', din1],  
    # ['eff.TruthTau_LeadingTrk_d0_sn1.json', din1],  
    # ['eff.TruthTau_LeadingTrk_d0_n2.json', din1]
#['eff.TruthTau_LeadingTrk_d0_s1.json',din1],
#['eff.TruthTau_LeadingTrk_d0_s3.json',din1],
#['eff.TruthTau_LeadingTrk_d0_s2.json',din1],
#['eff.TruthTau_LeadingTrk_d0_s4.json',din1],
#['eff.TruthTau_Lxy_s1.json',lin1],
#['eff.TruthTau_Lxy_s3.json',lin1],
#['eff.TruthTau_Lxy_s2.json',lin1],
#['eff.TruthTau_Lxy_s4.json',lin1],
    #['eff.TruthTau_Vis_Pt_n1.json', ptbin]
    #['eff.TruthTau_leading_LeadingTrk_d0_1.json',din1],
    #['eff.TruthTau_leading_Lxy_1.json',lin1],
    #['eff.TruthTau_leading_LeadingTrk_d0_2.json',din1],
    #['eff.TruthTau_leading_Lxy_2.json',lin1],
    #['eff.TruthTau_leading_LeadingTrk_d0_3.json',din1],
    #['eff.TruthTau_leading_Lxy_3.json',lin1],
    #['eff.TruthTau_leading_LeadingTrk_d0_4.json',din1],
    #['eff.TruthTau_leading_Lxy_4.json',lin1],

#    ['eff.TruthTau_LeadingTrk_d0_1.json',din1],
#    ['eff.TruthTau_Lxy_1.json',lin1],
#    ['eff.TruthTau_LeadingTrk_d0_2.json',din1],
#    ['eff.TruthTau_Lxy_2.json',lin1],
#    ['eff.TruthTau_LeadingTrk_d0_3.json',din1],
#    ['eff.TruthTau_Lxy_3.json',lin1],
#    ['eff.TruthTau_LeadingTrk_d0_4.json',din1],
#    ['eff.TruthTau_Lxy_4.json',lin1],

['eff.TruthTau_leading_LeadingTrk_d0_1.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_2.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_3.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_4.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_l1.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_l2.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_l3.json',din1],
['eff.TruthTau_leading_LeadingTrk_d0_l4.json',din1],
['eff.TruthTau_leading_Lxy_1.json',lin1],
['eff.TruthTau_leading_Lxy_2.json',lin1],
['eff.TruthTau_leading_Lxy_3.json',lin1],
['eff.TruthTau_leading_Lxy_4.json',lin1],
['eff.TruthTau_leading_Lxy_l1.json',lin1],
['eff.TruthTau_leading_Lxy_l2.json',lin1],
['eff.TruthTau_leading_Lxy_l3.json',lin1],
['eff.TruthTau_leading_Lxy_l4.json',lin1],

    ['eff.TruthTau_Vis_Pt_1.json', ptbin],
    ['eff.TruthTau_Vis_Pt_2.json', ptbin],
    ['eff.TruthTau_Vis_Pt_3.json', ptbin],
    ['eff.TruthTau_Vis_Pt_4.json', ptbin],

    #['eff.TruthTau_Lxy_ssm.json', lin1], 
    #['eff.TruthTau_Lxy_st1.json', lin1], 
    #['eff.TruthTau_Lxy_sn1.json', lin1], 
    #['eff.TruthTau_Lxy_st2.json', lin1],
]


processLst = []
for pargs in arglist:
    # draw(*pargs)
    processLst.append(Process(target=draw, args=(pargs)))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
