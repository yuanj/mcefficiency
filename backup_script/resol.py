import ROOT
import os
from collections import defaultdict

output = 'output_res.root'
jer = defaultdict(list)
for root, dirs, files in os.walk('run'):
    if 'data-ANALYSIS' in root:
        for file in files:
            name = root.split('/')[1].rsplit('_',1)[0]
            f = ROOT.TFile.Open(os.path.join(root, file))
            for event in f.analysis:
                jer_lst = [ pReco/pTruth for pTruth, pReco in zip(event.Matched_TruthTau_Vis_Pt, event.Matched_RecTau_Pt)]
                jer[name].append([event.Weight, jer_lst])

new_file = ROOT.TFile(output, "RECREATE")
for k,v in jer.items():
    hist = ROOT.TH1F(k, k, 30, 0.7, 1.3)
    for e in v:
        for r in e[1]:
            hist.Fill(r, e[0])
    hist.Write()
new_file.Close()


#colors = [ROOT.kOrange, ROOT.kRed, ROOT.kPink+7, ROOT.kMagenta, ROOT.kViolet, ROOT.kBlue, ROOT.kAzure, ROOT.kCyan, ROOT.kTeal, ROOT.kGreen, ROOT.kSpring, ROOT.kYellow+1, ROOT.kBlack+2, ROOT.kRed+2]
colors = [ROOT.kBlack, ROOT.kOrange, ROOT.kRed, ROOT.kBlue, ROOT.kGreen, ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]
input_file = ROOT.TFile(output)
canvas = ROOT.TCanvas("canvas", "Canvas Title", 1600, 1200)
legend = ROOT.TLegend(0.7, 0.4, 0.9, 0.9)
for i, key in enumerate(input_file.GetListOfKeys()):
    obj = key.ReadObj()
    hist = input_file.Get(key.ReadObj().GetName())
    hist.SetLineColor(colors[i])
    hist.Scale(1/hist.Integral())
    if i==0:
        hist.Draw("HIST")
        hist.SetStats(0)
        hist.SetTitle("")
        hist.SetXTitle("pReco/pTruth")
        hist.SetYTitle("Events")
    else:
        hist.Draw("HISTSAME")
    #max_value = hist.GetMaximum()
    hist.SetMaximum(0.15)
    #hist.SetMaximum(8000)

    legend.AddEntry(hist, key.ReadObj().GetName(), "l")
legend.Draw()
canvas.SaveAs(output+".png")
input_file.Close()
