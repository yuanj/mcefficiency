import time
import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process, Queue
import pickle
from array import array
import json

def find_max_subsh_number(directory):
    max_number = -1
    for filename in os.listdir(directory):
        full_path = os.path.join(directory, filename)
        if re.match(r'eff.sub\.sh\.\d+', filename):
            number = int(re.search(r'\d+$', filename).group())
            max_number = max(max_number, number)
    return max_number

def run(func, argList):
    out = []
    for args in argList:
        out.append(func(*args))
    return out

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return "gmsb_"+re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Stau_" in fullName:
        match = re.match(r".*Stau_(.00)_50_.33_(.*ns).*", folder)
        return "lle_"+match.groups()[0]+"_"+match.groups()[1]
        #return "lle_"+match.groups()[1]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName and ("ttbar_allhad" not in fullName):
        #return re.findall(r".*Py8_(ttbar_.*)_ANA.*", folder)[0]
        return re.findall(r".*Py8_(.*)_.*_ANA.*", folder)[0]
    else:
        return "ttbar_allhad"

def plotEff(pattern, truth_branch, match_branch, nick_name, binning, ptcut=40e3, cut_branch="no", cfun="abs", clow=0, chigh=1e3):
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2'
    treename = 'analysis'

    processes = defaultdict(list)

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        if "StauStau_directLLP" in root: continue
        procName = getProc(root)
        r_files = filter(lambda x: "root" in x, files)
        for i, ifile in enumerate(r_files):
            # if i>20: break

            num = find_max_subsh_number('.')+1
            with open('eff.sub.sh.'+str(num),'w') as f:
                f.write('python real_job_calc_eff.py '+' '.join([root, ifile, truth_branch, match_branch, str(ptcut), cut_branch, cfun, str(clow), str(chigh)]))
            os.system('chmod u+x eff.sub.sh.'+str(num))

            outname = "eff."+procName+"."+truth_branch+"_pt"+str(ptcut/1e3)+"_"+cut_branch+str(float(clow))+str(float(chigh))+"_"+ifile+".pk"
            processes[procName].append(outname)
    with open('eff.'+truth_branch+'_'+nick_name+".json", "w") as outfile:
        json.dump(processes, outfile)
    return 'eff.'+truth_branch+'_'+nick_name+".json"
if __name__ == '__main__':
    pattern1 = ".*Testn03.*"#StauStau_.*"
    patterns = ".*Testn04.*Stau_.*"#StauStau_.*"
    patternz = ".*Testn03.*Ztautau.*"#StauStau_.*"
    patternt = ".*Testn03.*ttbar.*"#StauStau_.*"
    
    lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
    din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
    
    dinv0 = array('f', [-6,-1.5,-1,0,4])
    dinv1 = array('f', [-6,-1.5,-1,-0.5,0,0.5,1,4])
    dinv2 = array('f', [-6,-1.5,-1,-0.5,0,1,4])
    dinv3 = array('f', [-6,-0.5,0,1,2,4])
    dinv4 = array('f', [-6,0,2,2.5,4])
    
    argLst=[
        # the very start point
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n0", [10, -1, 4], 40e3),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n0", [12, -4, 2], 40e3),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n1", [14, -3, 4], 40e3),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n1", [20, -6, 4], 40e3),
        
        # 1-d efficiency with good binning.
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n2", [len(lin1)-1, lin1 ], 40e3),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2", [len(din1)-1, din1], 40e3),
        #(patternz, "TruthTau_Lxy", "TruthTau_IsMatched", "n2z", [len(lin1)-1, lin1 ], 40e3),
        #(patternz, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2z", [len(din1)-1, din1], 40e3),
        #(patternt, "TruthTau_Lxy", "TruthTau_IsMatched", "n2t", [len(lin1)-1, lin1 ], 40e3),
        #(patternt, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n2t", [len(din1)-1, din1], 40e3),
    
        # efficiency dependence on pt 
        #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n1", [16, 40, 200], 40e3),
        #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n2", [16, 40, 200], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -10,-0.5),
        #(pattern1, "TruthTau_Vis_Pt", "TruthTau_IsMatched", "n3", [16, 40, 200], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -0.5),
    
        # 2d check for d0 and Lxy
        # separate displaced and prompt signal tau by d0 
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v0", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -6,-3),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v1", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -3,-2),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v2", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -2,-1.5),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v3", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -1.5,-1),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v4", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -1,-0.5),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v5", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), -0.5,0),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v6", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 0,0.5),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v7", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 0.5,1),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v8", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 1,1.5),
        #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "v9", [len(lin1)-1, lin1 ], 40e3, "TruthTau_LeadingTrk_d0", lambda x: math.log(abs(x)+1e-10, 10), 1.5,4),
    
        # 2d check for d0 and Lxy
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v0", [len(dinv0)-1, dinv0], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), -3,-0.5),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v1", [len(dinv0)-1, dinv0 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), -0.5,0),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v2", [len(dinv1)-1, dinv1 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 0,0.5),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v3", [len(dinv1)-1, dinv1 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 0.5,1),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v4", [len(dinv2)-1, dinv2 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 1,1.5),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v5", [len(dinv3)-1, dinv3 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 1.5,2),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v6", [len(dinv4)-1, dinv4 ], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 2,2.5),
        #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "v7", [1, -6, 4], 40e3, "TruthTau_Lxy", lambda x: math.log(abs(x)+1e-10, 10), 2.5,4),
    
        # check eta 
        #(pattern1, "TruthTau_Vis_Eta", "TruthTau_IsMatched", "n1", [13,0,2.6], 40e3),
        #(pattern1, "TruthTau_Vis_Eta", "TruthTau_IsMatched", "n2", [20,0,6], 40e3),
    
        # check efficiency separately
        #(patterns, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "s1", [len(din1)-1, din1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', -1, 1),
        #(patterns, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "s2", [len(din1)-1, din1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 0, 2),
        #(patterns, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "s3", [len(din1)-1, din1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 1, 3),
        #(patterns, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "s4", [len(din1)-1, din1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 2, 4),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "s1", [len(lin1)-1, lin1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', -1, 1),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "s2", [len(lin1)-1, lin1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 0, 2),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "s3", [len(lin1)-1, lin1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 1, 3),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "s4", [len(lin1)-1, lin1], 40e3, "TruthTau_LastSUSY_Mother_PDGID", 'dsid', 2, 4),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "ssm", [len(din1)-1, din1], 40e3, "TruthTau_Mother_PDGID", 'abs', 0, 1000000),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "st1", [len(din1)-1, din1], 40e3, "TruthTau_Mother_PDGID", 'abs', 1000014, 1000016),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "sn1", [len(din1)-1, din1], 40e3, "TruthTau_Mother_PDGID", 'abs', 1000021, 1000023),
        #(patterns, "TruthTau_Lxy", "TruthTau_IsMatched", "st2", [len(din1)-1, din1], 40e3, "TruthTau_Mother_PDGID", 'abs', 2000014, 2000016),
    
        #(".*Testn07.*StauRPCRPV_100.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "v1", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*StauRPCRPV_100.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "v1", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*StauRPCRPV_200.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "v2", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*StauRPCRPV_200.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "v2", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*StauRPCRPV_300.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "v3", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*StauRPCRPV_300.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "v3", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*StauRPCRPV_400.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "v4", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*StauRPCRPV_400.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "4", [len(din1)-1, din1], 40e3),

        #(".*Testn07.*Stau_100.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "1", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_100.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "1", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_200.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "2", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_200.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "2", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_300.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "3", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_300.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "3", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_400.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "4", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_400.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "4", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', -1, 1),
        #(".*Testn07.*Stau_100.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "l1", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_100.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "l1", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_200.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "l2", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_200.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "l2", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_300.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "l3", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_300.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "l3", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_400.*", "TruthTau_leading_Lxy", "TruthTau_leading_IsMatched", "l4", [len(lin1)-1, lin1 ], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),
        #(".*Testn07.*Stau_400.*", "TruthTau_leading_LeadingTrk_d0", "TruthTau_leading_IsMatched", "l4", [len(din1)-1, din1], 40e3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 'dsid', 0, 2),

        (".*Testn03.*Stau_100.*", "TruthTau_Vis_Pt", "TruthTau_IsMatched", "1", [16, 40, 200], 40e3),
        (".*Testn03.*Stau_200.*", "TruthTau_Vis_Pt", "TruthTau_IsMatched", "2", [16, 40, 200], 40e3),
        (".*Testn03.*Stau_300.*", "TruthTau_Vis_Pt", "TruthTau_IsMatched", "3", [16, 40, 200], 40e3),
        (".*Testn03.*Stau_400.*", "TruthTau_Vis_Pt", "TruthTau_IsMatched", "4", [16, 40, 200], 40e3),
        #(".*Testn07.*Stau_100.*", "TruthTau_Lxy", "TruthTau_IsMatched", "1", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*Stau_100.*", "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "1", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*Stau_200.*", "TruthTau_Lxy", "TruthTau_IsMatched", "2", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*Stau_200.*", "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "2", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*Stau_300.*", "TruthTau_Lxy", "TruthTau_IsMatched", "3", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*Stau_300.*", "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "3", [len(din1)-1, din1], 40e3),
        #(".*Testn07.*Stau_400.*", "TruthTau_Lxy", "TruthTau_IsMatched", "4", [len(lin1)-1, lin1 ], 40e3),
        #(".*Testn07.*Stau_400.*", "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "4", [len(din1)-1, din1], 40e3),
    
    ]
    os.system('rm -f eff.sub.sh.*')
    out = run(plotEff, argLst)
    print "Next step: draw plots with these files after condor jobs are done: "+", ".join(out)
    num = find_max_subsh_number('.')+1
    # print 'hep_sub eff.sub.sh."%{ProcId}" -g atlas -n '+str(num)
    os.system('hep_sub eff.sub.sh."%{ProcId}" -g atlas -n '+str(num))
    
