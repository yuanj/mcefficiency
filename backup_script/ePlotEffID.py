import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process

UserColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, ROOT.kRed, ROOT.kOrange, ROOT.kPink+7, ROOT.kMagenta, ROOT.kViolet, ROOT.kAzure,ROOT.kCyan, ROOT.kTeal,ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName:
        return re.findall(r".*Py8_(.*)_.*_ANA.*", folder)[0]

def flatten(up, dw, cut, weights, ids):
    # medium reco tau over truth tau 
    targetList = []
    effList = []
    cutList = []
    weiList = []
    for ievt in range(len(dw)):
        if len(dw[ievt])!=len(cut[ievt]): continue
        eff = 0
        for imat, matched_d0 in enumerate(up[ievt]):
            if ids[imat]>=2:
                eff = 1
            else:
                eff = 0
            targetList.append(matched_d0)
            effList.append(eff)
            cutList.append(cut[ievt][0]) # xx
            weiList.append(weights[ievt])
    return targetList, effList, cutList, weiList

def plotEff(branch_dw, NbinsX, Xmin, Xmax):
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/ResEff/Ntuples2'
    pattern = ".*Test_v03.*StauStau_.*"
    pattern = ".*Test_v03.*"
    #branch_dw = "TruthTau_d0"
    branch_up = 'Matched_'+branch_dw
    branch_lxy = ['TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly']
    treename = 'analysis'
    xfunc = lambda x: math.log(abs(x)+1e-10)
    
    upvarDict = defaultdict(list)
    dwvarDict = defaultdict(list)
    weiDict = defaultdict(list)
    cutDict = defaultdict(list)
    idDict = defaultdict(list)
    
    for root, dirs, files in os.walk(sourceDir):
        procName = getProc(root)
        if not re.search(pattern, root.split('/')[-1]): continue
        for file in files:
            file = ROOT.TFile.Open(os.path.join(root, file))
            for event in file.analysis:
                idDict[procName].append(list(getattr(event,'Matched_RecTau_ID')))
                weiDict[procName].append(getattr(event,'Weight'))
                upvarDict[procName].append(list(getattr(event,branch_up)))
                dwvarDict[procName].append(list(getattr(event,branch_dw)))
                lxy_evt = []
                for lx,ly in zip(getattr(event,branch_lxy[0]), getattr(event,branch_lxy[1])):
                    lxy_evt.append(math.sqrt(lx*lx+ly*ly))
                cutDict[procName].append(lxy_evt)
                if "StauStau_" not in root and len(weiDict[procName])>1e5: break
            if "StauStau_" not in root: break
    
    sortedName = sorted(cutDict);
    canvas = ROOT.TCanvas(branch_up+"canvas", branch_up+"Canvas Title", 1600, 1200)
    isFirst = True
    legend = ROOT.TLegend(0.7, 0.8, 0.9, 0.9)
    tmp_store=[]
    for i, process in enumerate(sortedName):
        branchList, effList, cutList, weiList = flatten(upvarDict[process],dwvarDict[process],cutDict[process],weiDict[process], idDict[procName])
        sumwei = np.array(weiList).sum()
        weiList = weiList / sumwei
        hist = ROOT.TH1F(process+branch_up, process+branch_up, NbinsX, Xmin, Xmax)
        hist_down = ROOT.TH1F(process+branch_up+"down", process+branch_up+"down", NbinsX, Xmin, Xmax)
        print "There are ",len(branchList)," raw number for ",process
        for itau in range(len(branchList)):
            hist.Fill(xfunc(branchList[itau]), weiList[itau]*effList[itau])
            hist_down.Fill(xfunc(branchList[itau]), weiList[itau])
        hist.Divide(hist_down)
        hist.SetLineColor(UserColors[i])
        hist.SetLineWidth(5)
        if isFirst:
            hist.Draw()
            hist.SetXTitle(branch_dw)
            hist.SetYTitle("efficiency")
            hist.SetTitle(branch_dw)
            hist.GetYaxis().SetRangeUser(0., 1.);
            isFirst = False
        else:
            hist.Draw('SAME')
        hist.SetStats(0)
        legend.AddEntry(hist, process, "l")
        tmp_store.append(hist)
    legend.Draw()
    canvas.SaveAs("IDEFF1_"+branch_up+".png")

def submit(PlotHist, argLst):
    processLst=[]
    for argTuple in argLst:
        processLst.append(Process(target=PlotHist, args=argTuple))
        processLst[-1].start()
    for proc in processLst:
        proc.join()

argLst=[
    ("TruthTau_Lxy", 16, -2, 6),
    ("TruthTau_d0", 24, -8, 4)
]
submit(plotEff, argLst)
