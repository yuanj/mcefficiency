import time
import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process, Queue
import subprocess
import pickle

def plotEff(pattern, truth_branch, match_branch, nick_name, NbinsX, Xmin, Xmax, NbinsY, Ymin, Ymax, cut_branch="", cfunc=lambda x:x/1e3, clow=0, chigh=1e3):
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc/Ntuples2'
    # treename = 'analysis'
    # xfunc = lambda x: math.log(abs(x)+1e-10, 10)
    j = 0
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for i, ifile in enumerate(files):
            # submit hep_sub job, 1 job for 1 file
            # fill up, down TH2F
            with open('sub.sh.'+str(j),'w') as f:
                f.write(' '.join(['python workspace_sub.py', root.split('/')[-1]+"_"+str(i), truth_branch, match_branch, nick_name, str(NbinsX), str(Xmin), str(Xmax), str(NbinsY), str(Ymin), str(Ymax), cut_branch, '40e3', '1e7']))
            os.system('chmod u+x sub.sh.'+str(j))
            j+=1
            #subprocess.run(['hep_sub', 'sub.sh.'+str(j)])



pattern1 = ".*Testn02.*"#StauStau_.*"
plotEff(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n1", 32, -5, 3, 200, 0, 1000, "TruthTau_Vis_Pt", '', 40)

#pattern1 = ".*Testn02.*"#StauStau_.*"
##pattern2 = ".*Test_v03.*"
#argLst=[
#    #(pattern1, "TruthTau_Lxy", "TruthTau_IsMatched", "n1", 32, -8, 8, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 40),
#    #(pattern1, "TruthTau_d0", "TruthTau_IsMatched", "n1", 32, -5, 3, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 40),
#    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n1", 32, -5, 3, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 40),
#    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n148", 32, -5, 3, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 40,80),
#    (pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n180", 32, -5, 3, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 80,100),
#    #(pattern1, "TruthTau_LeadingTrk_d0", "TruthTau_IsMatched", "n10", 32, -5, 3, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 100),
#]
#submit(plotEff, argLst)

