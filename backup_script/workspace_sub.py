import time
import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process, Queue
import pickle

def flat_filter(truth_branch, match_branch, Weight, passcut):
    xArray, effArray, weiArray = [], [], []
    for val, ismatched, ispassed in zip(truth_branch, match_branch, passcut):
        if ispassed:
            xArray.append(val)
            effArray.append(ismatched)
            weiArray.append(Weight)
    return xArray, effArray, weiArray

def calc_eff(num, root, ifile, truth_branch, match_branch, cut_branch, cfunc, clow, chigh):
    outname = "ieffcache_"+truth_branch+"_"+"".join(cut_branch)+str(clow)+str(chigh)+"_"+ifile+".pk"
    procName = getProc(root)
    if os.path.isfile(outname):
        return procName, outname
    file = ROOT.TFile.Open(os.path.join(root, ifile))
    nevt = 0
    start = time.time()
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:
        nevt += 1
        passcut= []
        if cut_branch!="":
            for cvalues in zip(*[getattr(event, cbr) for cbr in cut_branch]):
                icut = cfunc(*cvalues)
                passcut.append((icut>clow and icut<chigh))
        else:
            passcut = [True]*len(list(getattr(event, truth_branch)))
        ixArray, ieffArray, iweiArray = flat_filter(list(getattr(event, truth_branch)), list(getattr(event, match_branch)), getattr(event,'Weight'), passcut)
        xArray.extend(ixArray)
        effArray.extend(ieffArray)
        weiArray.extend(iweiArray)
    end = time.time()
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    #print "Read",nevt,"events cost",end-start,"s, get",len(xArray),"entries,save to",outname
    return procName, outname
    #return procName, xArray, effArray, weiArray
