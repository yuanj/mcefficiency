import ROOT
import re
import pickle
import os
from doPlotEff import getProc
    
def flat_filter(truth_branch, match_branch, Weight, passcut):
    xArray, effArray, weiArray = [], [], []
    for val, ismatched, ispassed in zip(truth_branch, match_branch, passcut):
        if ispassed:
            xArray.append(val)
            effArray.append(ismatched)
            weiArray.append(Weight)
    # weiArray2 = [i/len(weiArray)for i in weiArray]
    return xArray, effArray, weiArray

def dsid_transfer(dsid):
    if abs(dsid)==1000015 or abs(dsid)==2000015:
        return 0 # stau
    elif abs(dsid)==1000022:
        return 1 #N1
    elif abs(dsid)<1000: 
        return 2
    else:
        return 3
def calc_eff(root, ifile, truth_branch, match_branch, ptcut, cut_branch, cfun_name, clow, chigh):
    procName = getProc(root)
    outname = "eff."+procName+"."+truth_branch+"_pt"+str(ptcut/1e3)+"_"+cut_branch+str(float(clow))+str(float(chigh))+"_"+ifile+".pk"
    # if os.path.isfile(outname):
    #     return procName, outname
    cfun_dict={
        "log":lambda x: math.log(abs(x)+1e-10, 10),
        "abs":lambda x : abs(x),
        "dsid":dsid_transfer
    }
    cfun = cfun_dict[cfun_name]

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:

        ptpass = [ipt>ptcut for ipt in getattr(event, 'TruthTau_Vis_Pt')]
        if cut_branch!="no":
            passcut = [(cfun(val)>clow and cfun(val)<chigh) for val in getattr(event, cut_branch)]
            passcut = [(p and c) for p, c in zip(ptpass, passcut)]
        else:
            passcut = ptpass

        ixArray, ieffArray, iweiArray = flat_filter(list(getattr(event, truth_branch)), list(getattr(event, match_branch)), getattr(event,'Weight'), passcut)

        xArray.extend(ixArray)
        effArray.extend(ieffArray)
        weiArray.extend(iweiArray)
        if 'Stau' not in root:
            if len(weiArray)>1e5: 
                break

    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname
def calc_eff_ld(root, ifile, truth_branch, match_branch, ptcut, cut_branch, cfun_name, clow, chigh):
    procName = getProc(root)
    outname = "eff."+procName+"."+truth_branch+"_pt"+str(ptcut/1e3)+"_"+cut_branch+str(float(clow))+str(float(chigh))+"_"+ifile+".pk"
    cfun_dict={
        "log":lambda x: math.log(abs(x)+1e-10, 10),
        "abs":lambda x : abs(x),
        "dsid":dsid_transfer
    }
    cfun = cfun_dict[cfun_name]

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:

        ptpass = getattr(event, 'TruthTau_leading_Pt') > ptcut
        if cut_branch!="no":
            passcut = (cfun(getattr(event, cut_branch)) >clow) and (cfun(getattr(event, cut_branch))<chigh) and ptpass
        else:
            passcut = ptpass

        if passcut:
            xArray.append(getattr(event, truth_branch))
            effArray.append(getattr(event, match_branch))
            weiArray.append(getattr(event,'Weight'))

        if 'Stau' not in root:
            if len(weiArray)>1e5: 
                break

    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname

import argparse
parser = argparse.ArgumentParser(description='Calculate efficiency')
parser.add_argument('root', type=str, help='Root directory')
parser.add_argument('ifile', type=str, help='Input file')
parser.add_argument('truth_branch', type=str, help='Truth branch name')
parser.add_argument('match_branch', type=str, help='Match branch name')
parser.add_argument('ptcut', type=float, help='PT cut value')
parser.add_argument('cut_branch', type=str, help='Cut branch name')
parser.add_argument('cfun_name', type=str, help='CFUN name')
parser.add_argument('clow', type=float, help='CLow value')
parser.add_argument('chigh', type=float, help='CHigh value')
args = parser.parse_args()

if "leading" in args.truth_branch:
    calc_eff_ld(args.root, args.ifile, args.truth_branch, args.match_branch, args.ptcut, args.cut_branch, args.cfun_name, args.clow, args.chigh)
else:
    calc_eff(args.root, args.ifile, args.truth_branch, args.match_branch, args.ptcut, args.cut_branch, args.cfun_name, args.clow, args.chigh)
