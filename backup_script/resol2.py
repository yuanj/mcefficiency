import ROOT
import os
from collections import defaultdict
import math

output = 'output_res.root'
jer = defaultdict(list)
for root, dirs, files in os.walk('run'):
    if 'data-ANALYSIS' in root:
        for file in files:
            name = root.split('/')[1].rsplit('_',1)[0]
            f = ROOT.TFile.Open(os.path.join(root, file))
            for event in f.analysis:
                for ind in range(0, len(event.Matched_TruthTau_Vis_Pt)):
                    jer[name].append({"weight":event.Weight, "ratio":event.Matched_RecTau_Pt[ind]/event.Matched_TruthTau_Vis_Pt[ind], 
                        "d0": math.log(abs(event.Matched_TruthTau_d0[ind])), "z0": math.log(abs(event.Matched_TruthTau_z0[ind]))
                        })

def plot2(k, v, x_axis, y_axis):
    drange={
            "d0": [20,-14,6],
            "z0": [24,-6,6],
            "ratio":[20,0,2],
            }
    axis_title = {
            "d0": "log_abs_taud0",
            "z0": "log_abs_tauz0",
            "ratio":"pTreco/pTruth",
            }
    NbinsX, Xmin, Xmax = drange[x_axis]
    NbinsY, Ymin, Ymax = drange[y_axis]

    canvas = ROOT.TCanvas(k+"canvas", k+"Canvas Title", 1600, 1200)
    hist = ROOT.TH2F(k, k, NbinsX, Xmin, Xmax, NbinsY, Ymin, Ymax)
    for event in v:
        hist.Fill(event[x_axis], event[y_axis], event['weight'])
    hist.Scale(1.0 / hist.Integral())
    hist.Draw('COLZ')

    labels=[]
    for i in range(1, hist.GetNbinsX() + 1):
        for j in range(1, hist.GetNbinsY() + 1):
            x = hist.GetXaxis().GetBinCenter(i)
            y = hist.GetYaxis().GetBinCenter(j)
            content = round(hist.GetBinContent(i, j), 2)
            if content>0:
                label = ROOT.TLatex(x, y, str(content))
                label.SetTextAlign(22)
                label.SetTextSize(0.02)
                label.Draw()
                labels.append(label)

    hist.SetXTitle(axis_title[x_axis])
    hist.SetYTitle(axis_title[y_axis])
    hist.SetStats(0)
    hist.SetTitle(k)
    canvas.SaveAs("res_"+x_axis+"_"+y_axis+"_"+k+".png")

plane = [
        ('d0','ratio'),
        ('z0','ratio'),
        ]

allv = []
for v in jer.values():
    allv.extend(v)
for choice in plane:
    plot2('all', allv, choice[0], choice[1])
    for k,v in jer.items():
        plot2(k,v, choice[0], choice[1])
