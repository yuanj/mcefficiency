import ROOT
import os
from collections import defaultdict
import math

sourceDir='Ntuples'

def PlotHist(d0):
    output = 'output_'+d0+'.root'
    jer = defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        if 'Test_v01' in root:
            for file in files:
                #if '_200_' in root or '_400_' in root: continue
                name = root.split('/')[1]#.rsplit('_',1)[0]
                if 'Stau' not in name: continue
                f = ROOT.TFile.Open(os.path.join(root, file))
                for event in f.analysis:
                    jer[name].append([event.Weight, [x for x in getattr(event,d0)]])
    
    new_file = ROOT.TFile(output, "RECREATE")
    for k,v in jer.items():
        hist = ROOT.TH1F(k, k, 80, -10, 6)
        for e in v:
            for d in e[1]:
                if 'd0' in output:
                    hist.Fill(math.log(abs(d)), e[0])
                else:
                    hist.Fill(abs(d), e[0])
        hist.Write()
    new_file.Close()
    
    
    colors = [ROOT.kBlack, ROOT.kOrange, ROOT.kRed, ROOT.kBlue, ROOT.kGreen, ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]
    #colors = [ROOT.kOrange, ROOT.kRed, ROOT.kPink+7, ROOT.kMagenta, ROOT.kViolet, ROOT.kBlue, ROOT.kAzure, ROOT.kCyan, ROOT.kTeal, ROOT.kGreen, ROOT.kSpring, ROOT.kYellow+1, ROOT.kBlack+2, ROOT.kRed+2]
    input_file = ROOT.TFile(output)
    canvas = ROOT.TCanvas("canvas", "Canvas Title", 1600, 1200)
    legend = ROOT.TLegend(0.7, 0.6, 0.9, 0.9)
    for i, key in enumerate(input_file.GetListOfKeys()):
        obj = key.ReadObj()
        hist = input_file.Get(key.ReadObj().GetName())
        hist.SetLineColor(colors[i])
        hist.Scale(1/hist.Integral())
        if i==0:
            hist.Draw("HIST")
            hist.SetStats(0)
            hist.SetTitle("")
            if 'd0' in output:
                hist.SetXTitle("log_abs_"+d0)
            else:
                hist.SetXTitle(d0)
            hist.SetYTitle("Events")
        else:
            hist.Draw("HISTSAME")
        if 'd0' in output:
            hist.SetMaximum(0.1)
        else:
            hist.SetMaximum(1)
    
        legend.AddEntry(hist, key.ReadObj().GetName(), "l")
    
    #ROOT.gPad.SetLogy(1)
    legend.Draw()
    canvas.SaveAs(output+".png")
    input_file.Close()

PlotHist('Matched_TruthTau_Lxy')
PlotHist('Matched_TruthTau_Lz')

# PlotHist('TruthTau_d0')
# PlotHist('Matched_RecTau_Leading_Trk_d0')
# PlotHist('Matched_TruthTau_d0')
# #PlotHist('Matched_RecTau_ChargedTrk')
# PlotHist('TruthTau_Num')
