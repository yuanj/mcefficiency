import ROOT
import os
from collections import defaultdict
import math
import re
import numpy as np
from multiprocessing import Process

UserColors = [ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, ROOT.kRed, ROOT.kOrange, ROOT.kTeal, ROOT.kViolet, ROOT.kPink+7, ROOT.kMagenta, ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2]

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName:
        return re.findall(r".*Py8_(ttbar_.*)_ANA.*", folder)[0]
        #return re.findall(r".*Py8_(.*)_.*_ANA.*", folder)[0]

def flatten(up, dw, cut, weights, ids):
    targetList = []
    effList = []
    cutList = []
    weiList = []
    for ievt in range(len(dw)):
        if len(dw[ievt])!=len(cut[ievt]): continue
        for itau, truth_d0 in enumerate(dw[ievt]):
            eff = 0
            for imat, matched_d0 in enumerate(up[ievt]):
                if truth_d0 == matched_d0:
                    eff = 1
                    break
            targetList.append(truth_d0)
            effList.append(eff)
            cutList.append(cut[ievt][itau])
            weiList.append(weights[ievt])
    return targetList, effList, cutList, weiList
def effcalc(up, down, weight, cuts):
    xArray, effArray, weiArray = [], [], []
    for itau, truth_d0 in enumerate(down):
        if not cuts[itau] : continue
        eff = 0
        for imat, matched_d0 in enumerate(up):
            if truth_d0 == matched_d0:
                eff = 1
                break
        xArray.append(truth_d0)
        effArray.append(eff)
        weiArray.append(weight)
    return xArray, effArray, weiArray

def plotEff(pattern, branch_dw, nick_name, NbinsX, Xmin, Xmax, branch_cut="", cfunc=lambda x:x/1e3, clow=0, chigh=1e3):
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/ResEff/Ntuples2'
    branch_up = 'Matched_'+branch_dw
    #branch_lxy = ['TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly']
    treename = 'analysis'
    xfunc = lambda x: math.log(abs(x)+1e-10)
    
    effvarDict = defaultdict(list)
    xvarDict = defaultdict(list)
    weiDict = defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        procName = getProc(root)
        if not re.search(pattern, root.split('/')[-1]): continue
        for ifile in files:
            file = ROOT.TFile.Open(os.path.join(root, ifile))
            for event in file.analysis:
                lxy_evt = []
                if branch_cut!="":
                    for cvalues in zip(*[getattr(event, cbr) for cbr in branch_cut]):
                        icut = cfunc(*cvalues)
                        lxy_evt.append((icut>clow and icut<chigh))
                else:
                    lxy_evt = [True]*len(list(getattr(event,branch_dw)))
                xArray, effArray, weiArray = effcalc(list(getattr(event,branch_up)), list(getattr(event,branch_dw)), getattr(event,'Weight'), lxy_evt)
                xvarDict[procName].extend(xArray)
                effvarDict[procName].extend(effArray)
                weiDict[procName].extend(weiArray)
                if "StauStau_" not in root and len(weiDict[procName])>1e5: break
                #print "length",procName, len(weiDict[procName])
            print "From ", ifile, "read some events, current len:", len(weiDict[procName])
            if "StauStau_" not in root: break
    
    sortedName = sorted(xvarDict);
    canvas = ROOT.TCanvas(branch_up+"canvas", branch_up+"Canvas Title", 1600, 1200)
    isFirst = True
    legend = ROOT.TLegend(0.7, 0.65, 0.9, 0.9)
    tmp_store=[]
    for i, process in enumerate(sortedName):
        print "There are ",len(xvarDict[process])," raw number for ",process
        xbranchList, xeffList, xweiList = xvarDict[process], effvarDict[process], weiDict[process]
        sumwei = np.array(xweiList).sum()
        xweiList = xweiList / sumwei
        hist = ROOT.TH1F(process+branch_up, process+branch_up, NbinsX, Xmin, Xmax)
        hist_down = ROOT.TH1F(process+branch_up+"down", process+branch_up+"down", NbinsX, Xmin, Xmax)
        for itau in range(len(xbranchList)):
            hist.Fill(xfunc(xbranchList[itau]), xweiList[itau]*xeffList[itau])
            hist_down.Fill(xfunc(xbranchList[itau]), xweiList[itau])
        hist.Divide(hist_down)
        hist.SetLineColor(UserColors[i])
        hist.SetLineWidth(5)
        if isFirst:
            hist.Draw()
            hist.SetXTitle(branch_dw)
            hist.SetYTitle("efficiency")
            hist.SetTitle(branch_dw)
            hist.GetYaxis().SetRangeUser(0., 1.);
            isFirst = False
        else:
            hist.Draw('SAME')
        hist.SetStats(0)
        legend.AddEntry(hist, process, "l")
        tmp_store.append(hist)
    legend.Draw()
    canvas.SaveAs("EFF1_"+nick_name+branch_up+".png")

def submit(PlotHist, argLst):
    processLst=[]
    for argTuple in argLst:
        processLst.append(Process(target=PlotHist, args=argTuple))
        processLst[-1].start()
    for proc in processLst:
        proc.join()

pattern1 = ".*Test_v03.*StauStau_.*"
pattern2 = ".*Test_v03.*"
argLst=[
    #(pattern1, "TruthTau_Lxy", "v0_", 16, -2, 6),
    #(pattern1, "TruthTau_d0", "v0_",24, -8, 4),
    (pattern2, "TruthTau_d0", "v1_30", 24, -8, 4, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 0, 30),
    (pattern2, "TruthTau_d0", "v1_3050", 24, -8, 4, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 30, 50),
    (pattern2, "TruthTau_d0", "v1_50100", 24, -8, 4, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 50, 100),
    (pattern2, "TruthTau_d0", "v1_100", 24, -8, 4, ["TruthTau_Vis_Pt"], lambda x : x/1e3, 100, 1000)
]
submit(plotEff, argLst)
