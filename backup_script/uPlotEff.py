import uproot
import math
import matplotlib.pyplot as plt
from collections import defaultdict
import os
import re
import numpy as np
from multiprocessing import Process

treename = 'analysis'
sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/caiyc/RPV/EfficiencyStudy/xaodlearning/Ntuples/'

def flatten(x, weights):
    _weights = np.repeat(weights, [len(v) for v in x])
    _x = np.concatenate(x)
    return _x, _weights

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    else:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]

def PlotEff(pattern, variable, nickname='', axis=''):
    upvarDict = defaultdict(list)
    dwvarDict = defaultdict(list)
    weiDict = defaultdict(list)

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            procName = getProc(root)
            #procName = root.split('/')[-1].split('.')[-2].rsplit('_',1)[0] # .split('_',2)[2]
            fileHde = uproot.open(os.path.join(root, fileName))
            treeArr = fileHde[treename].arrays(['Weight', variable, 'Matched_'+variable])

            dwvarDict[procName].extend(treeArr[variable])
            upvarDict[procName].extend(treeArr['Matched_'+variable])
            weiDict[procName].extend(treeArr['Weight'])
    
    sortedName = sorted(weiDict);
    plt.figure()
    for procName in sortedName:
        upx, upw = flatten(upvarDict[procName], weiDict[procName])
        dwx, dww = flatten(dwvarDict[procName], weiDict[procName])
        if axis == "log":
            upx = [math.log(abs(i)+1e-9) for i in upx]
            dwx = [math.log(abs(i)+1e-9) for i in dwx]
        uphist, bins = np.histogram(upx, bins=20, weights=upw)
        print(uphist)
        dwhist, _  = np.histogram(dwx, weights=dww, bins=bins)
        hist = uphist / dwhist 
        xbins = [ (bins[i]+bins[i+1])/2 for i in range(len(bins)-1) ]
        plt.plot(xbins, hist, label=procName)
    plt.xlabel(axis+variable)
    plt.ylabel("Efficiency")
    plt.legend(loc='lower left')
    plt.savefig('Eff_'+nickname+variable+'.png')
    plt.close()

def PlotEffslices(pattern, nickname='', lowerlim = -20,upperlim = 10):
    # draw eff over d0, in different Lxy slices 
    upvarDict = defaultdict(list)
    dwvarDict = defaultdict(list)
    weiDict = defaultdict(list)
    upcutDict = defaultdict(list)
    dwcutDict = defaultdict(list)
    variable='TruthTau_d0'

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            procName = getProc(root)
            #procName = root.split('/')[-1].split('.')[-2].rsplit('_',1)[0] # .split('_',2)[2]
            fileHde = uproot.open(os.path.join(root, fileName))
            treeArr = fileHde[treename].arrays(['Weight', variable, 'Matched_'+variable, 'TruthTau_Lxy','Matched_TruthTau_Lxy'])
            dwvarDict[procName].extend(treeArr[variable])
            upvarDict[procName].extend(treeArr['Matched_'+variable])
            weiDict[procName].extend(treeArr['Weight'])
            upcutDict[procName].extend(treeArr['Matched_TruthTau_Lxy'])
            dwcutDict[procName].extend(treeArr['TruthTau_Lxy'])
    
    sortedName = sorted(weiDict);
    plt.figure()
    for procName in sortedName:
        upl, _   = flatten(upcutDict[procName], weiDict[procName])
        dwl, _   = flatten(dwcutDict[procName], weiDict[procName])
        upl = [math.log(abs(i)+1e-9) for i in upl]
        dwl = [math.log(abs(i)+1e-9) for i in dwl]

        upx, upw = flatten(upvarDict[procName], weiDict[procName])
        dwx, dww  = flatten(dwvarDict[procName], weiDict[procName])
        upx = [math.log(abs(i)+1e-9) for ind, i in enumerate(upx) if (upl[ind]>lowerlim and upl[ind]<upperlim)]
        upw = [ i for ind, i in enumerate(upw) if (upl[ind]>lowerlim and upl[ind]<upperlim)]

        dwx = [math.log(abs(i)+1e-9) for ind, i in enumerate(dwx) if (dwl[ind]>lowerlim and dwl[ind]<upperlim)]
        dww = [ i for ind, i in enumerate(dww) if (dwl[ind]>lowerlim and dwl[ind]<upperlim)]

        uphist, bins = np.histogram(upx, bins=10, weights=upw)
        dwhist, _  = np.histogram(dwx, weights=dww, bins=bins)
        hist = uphist / dwhist 
        xbins = [ (bins[i]+bins[i+1])/2 for i in range(len(bins)-1) ]
        plt.plot(xbins, hist, label=procName)
        plt.xlim(-15,5)
        plt.ylim(0,1.2)
    plt.title(str(lowerlim)+'<log Lxy<'+str(upperlim))
    plt.xlabel(variable)
    plt.ylabel("Efficiency")
    plt.legend(loc='lower left')
    plt.savefig('sEff_' + nickname + variable + str(upperlim) + '_' + str(lowerlim) + '.png')
    plt.close()

def PlotEffCut(pattern, variable, axis=''):
    # focus on d0 < 1 region
    upvarDict = defaultdict(list)
    dwvarDict = defaultdict(list)
    weiDict = defaultdict(list)
    ud0Dict = defaultdict(list)
    dd0Dict = defaultdict(list)

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            procName = root.split('/')[-1].split('.')[-2].rsplit('_',1)[0].split('_',2)[2]
            fileHde = uproot.open(os.path.join(root, fileName))
            treeArr = fileHde[treename].arrays(['Weight', variable, 'Matched_'+variable, 'Matched_TruthTau_d0', 'TruthTau_d0'])
            upvarDict[procName].extend(treeArr['Matched_'+variable])
            dwvarDict[procName].extend(treeArr[variable])
            weiDict[procName].extend(treeArr['Weight'])
            ud0Dict[procName].extend(treeArr['Matched_TruthTau_d0'])
            dd0Dict[procName].extend(treeArr['TruthTau_d0'])
    
    sortedName = sorted(weiDict);
    plt.figure()
    for procName in sortedName:
        upxLst, upwLst = flatten(upvarDict[procName], weiDict[procName])
        dwxLst, dwwLst = flatten(dwvarDict[procName], weiDict[procName])

        ud0Lst, _ = flatten(ud0Dict[procName], weiDict[procName])
        dd0Lst, _ = flatten(dd0Dict[procName], weiDict[procName])
        upx = [upxLst[i] for i, d0 in enumerate(ud0Lst) if abs(d0) < 1]
        upw = np.array([upwLst[i] for i, d0 in enumerate(ud0Lst) if abs(d0) < 1])
        dwx = [dwxLst[i] for i, d0 in enumerate(dd0Lst) if abs(d0) < 1]
        dww = np.array([dwwLst[i] for i, d0 in enumerate(dd0Lst) if abs(d0) < 1])

        if axis == "log":
            upx = [math.log(abs(i)) for i in upx]
            dwx = [math.log(abs(i)) for i in dwx]
        uphist, bins = np.histogram(upx, weights=upw)
        dwhist, _  = np.histogram(dwx, weights=dww, bins=bins)
        hist = uphist / dwhist 
        xbins = [ (bins[i]+bins[i+1])/2 for i in range(len(bins)-1) ]
        plt.plot(xbins, hist, label=procName)
    plt.xlabel(axis+variable)
    plt.ylabel("Efficiency")
    plt.legend(loc='lower left')
    plt.savefig('EffCd0_'+variable+'.png')
    plt.close()

def PlotEff2(pattern, variable1, variable2, axis='', xmin=-8, xmax=8, ymin=-12, ymax=8):
    upxvarDict = defaultdict(list)
    upyvarDict = defaultdict(list)

    dwxvarDict = defaultdict(list)
    dwyvarDict = defaultdict(list)
    
    weiDict = defaultdict(list)

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            # procName = root.split('/')[-1].split('.')[-2].rsplit('_',1)[0].split('_',2)[2] # target: 300_0_0p01ns
            procName = root.split('/')[-1].split('.')[-2].rsplit('_',1)[0].split('_',4)[4]
            print("source: "+root+', target: '+procName)
            fileHde = uproot.open(os.path.join(root, fileName))
            treeArr = fileHde[treename].arrays(['Weight', variable1, 'Matched_'+variable1, variable2, 'Matched_'+variable2])

            upxvarDict[procName].extend(treeArr['Matched_'+variable1])
            upyvarDict[procName].extend(treeArr['Matched_'+variable2])
            dwxvarDict[procName].extend(treeArr[variable1])
            dwyvarDict[procName].extend(treeArr[variable2])
            weiDict[procName].extend(treeArr['Weight'])
    
    sortedName = sorted(weiDict);
    plt.figure()
    for procName in sortedName:
        upx, upw = flatten(upxvarDict[procName], weiDict[procName])
        upy, __  = flatten(upyvarDict[procName], weiDict[procName])

        dwx, dww = flatten(dwxvarDict[procName], weiDict[procName])
        dwy, __  = flatten(dwyvarDict[procName], weiDict[procName])

        if axis == "log":
            upx = [math.log(abs(i)) for i in upx]
            upy = [math.log(abs(i)) for i in upy]
            dwx = [math.log(abs(i)) for i in dwx]
            dwy = [math.log(abs(i)) for i in dwy]

        uphist, xbins, ybins = np.histogram2d(upx, upy, bins=100, weights=upw, range=[[xmin, xmax], [ymin, ymax]])
        dwhist, _ , __ = np.histogram2d(dwx, dwy, weights=dww, bins=(xbins, ybins))
        hist = (uphist / dwhist).T

        plt.imshow(hist, interpolation='nearest', origin='lower', extent=[xbins[0], xbins[-1], ybins[0], ybins[-1]])
        # X, Y = np.meshgrid(xbins, ybins)
        # plt.pcolormesh(X, Y, hist)
        plt.colorbar()
        plt.xlabel(axis+variable1)
        plt.ylabel(axis+variable2)
        plt.title("Efficiency Map of "+procName)
        plt.savefig('Eff2_'+procName+"_"+variable1+"_"+variable2+'.png')
        plt.close()

# PlotEff2(".*Test_v01.*", 'TruthTau_Lxy', 'TruthTau_d0', 'log')
argLst=[
    #(".*Test_v01.*StauStau.*", 'TruthTau_Lxy', 'RPV', 'log'),
    (".*Test_v01.*StauStau.*", 'TruthTau_d0', 'RPV', 'log'),
    #(".*Test_v01.*Py8_.*", 'TruthTau_Lxy', 'SM', 'log'),
    #(".*Test_v01.*Py8_.*", 'TruthTau_d0', 'SM', 'log'),
    #(".*Test_v01.*", 'TruthTau_Lxy', 'all', 'log'),
    #(".*Test_v01.*", 'TruthTau_d0', 'all', 'log'),
]
processLst=[]
for argTuple in argLst:
    processLst.append(Process(target=PlotEff, args=argTuple))
    processLst[-1].start()
    # processLst.append(Process(target=PlotEffCut, args=argTuple))
    # processLst[-1].start()
for proc in processLst:
    proc.join()

#argLst=[
#(".*Test_v01.*StauStau.*", 'test',-100,100),
#(".*Test_v01.*StauStau.*", 'test',-8,-2),
#(".*Test_v01.*StauStau.*", 'test',-8,0),
#(".*Test_v01.*StauStau.*", 'test',0,2),
#(".*Test_v01.*StauStau.*", 'test',2,4),
#(".*Test_v01.*StauStau.*", 'test',4,10)
#]
#processLst=[]
#for argTuple in argLst:
#    processLst.append(Process(target=PlotEffslices, args=argTuple))
#    processLst[-1].start()
#for proc in processLst:
#    proc.join()
