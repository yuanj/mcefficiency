import uproot
import math
import matplotlib.pyplot as plt
from collections import defaultdict
import os
import re
import numpy as np
import pickle
from multiprocessing import Process, Queue
import time
from collections.abc import Iterable  

def log10(x):
    return math.log(abs(x)+1e-12,10)

def flatten(x, weights):
    try:
        _weights = np.repeat(weights, [len(v) for v in x])
        _x = np.concatenate(x)
        return _x, _weights
    except:
        return np.array(x), np.array(weights)


def flatten2(x, y, weights):
    try:
        _x, _y, _weights = [],[],[]
        for i in range(len(x)):
            if len(x[i])!=len(y[i]): 
                continue
            _weights.extend([weights[i]]*len(x[i]))
            _x.extend(x[i])
            _y.extend(y[i])
        return np.array(_x), np.array(_y), np.array(_weights)
    except:
        np.array(x), np.array(y), np.array(weights)

treename = 'analysis'
sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2'

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return "gmsb_"+re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
        #return re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
        #return re.findall(r".*StauStau_directLLP_(.00_0_.*ns).*", folder)[0]
    elif "Stau_" in fullName:
        return "lle_"+re.findall(r".*Stau_.00_50_.33_(.*ns).*", folder)[0]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName:
        return re.findall(r".*Py8_(.*)_.*_ANA.*", folder)[0]

def PlotHist(pattern, variable, nickName='', xfunc=lambda x: [i for i in x], nbins=16,  xmin=-8, xmax=8, norm = True):
    varDict = defaultdict(list)
    weiDict = defaultdict(list)
    EvtCount = defaultdict(int)
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            procName = getProc(root)
            fileHde = uproot.open(os.path.join(root, fileName))
            treeArr = fileHde[treename].arrays(['Weight', variable])
            varDict[procName].extend(treeArr[variable])
            weiDict[procName].extend(treeArr['Weight'])
            if "Stau" not in root:
                EvtCount[root.split('/')[-1]] += len(treeArr[variable])
                if EvtCount[root.split('/')[-1]]>2e5: 
                    break
                else:
                    print(root.split('/')[-1], EvtCount[root.split('/')[-1]])
    sortedName = sorted(varDict)
    plt.figure()
    for procName in sortedName:
        x, w = flatten(varDict[procName], weiDict[procName])
        if norm:
            sumwei = w.sum()
            w = w / sumwei
        x = xfunc(x)
        plt.hist(x, weights=w, bins=nbins, histtype='step', range=(xmin, xmax), label=procName)
    plt.title(nickName+variable)
    plt.legend(loc='upper left')
    plt.savefig('TH1_'+nickName+variable+'.png',dpi=200)
    plt.close()

def isubmit(PlotHist, argLst):
    start = time.time()
    processLst=[]
    queue = Queue()
    def wrapper(input, q1):
        a = PlotHist(*input)
        q1.put(a)
    for argTuple in argLst:
        processLst.append(Process(target=wrapper, args=(argTuple,queue)))
        processLst[-1].start()
    for i, proc in enumerate(processLst):
        proc.join()
        end = time.time()
        print("Process Status:",i,"/",len(processLst),", using",end-start)
    queue.put('done')
    outputs=[]
    #item=""
    while True:
        item=queue.get()
        if item== "done":
            break
        outputs.append(item)
    return outputs

def read_histo(root, fileName, variable, nickName, cut):
    # 1 job per file
    outName = "hist."+nickName+'.'+variable+"_"+fileName+".pk"
    procName = getProc(root)
    if os.path.isfile(outName):
        return procName, outName
    fileHde = uproot.open(os.path.join(root, fileName))
    if cut:
        treeArr = fileHde[treename].arrays(['Weight', variable, cut[0]])
    else:
        treeArr = fileHde[treename].arrays(['Weight', variable])
    if cut:
        xlist = list(treeArr[variable])
        wlist = list(treeArr['Weight'])
        clist = list(treeArr[cut[0]])
        xout, wout = [],[]
        for xevt, wevt, cevt in zip(xlist, wlist, clist):
            xtemp = []
            #wtemp = []
            for xtau, ctau in zip(xevt, cevt):
                if cut[1](ctau)>cut[2] and cut[1](ctau)<cut[3]:
                    xtemp.append(xtau)
                    #wtemp.append(wevt)
            xout.append(xtemp)
            wout.append(wevt)
        with open(outName, "wb") as pick:
            if len(xout)>2e5:
                pickle.dump({"label":procName, "xval":xout[:int(2e5)], "wei":wout[:int(2e5)]}, pick)
            else:
                pickle.dump({"label":procName, "xval":xout, "wei":wout}, pick)
    else:
        with open(outName, "wb") as pick:
            if len(list(treeArr[variable]))>2e5:
                pickle.dump({"label":procName, "xval":list(treeArr[variable])[:int(2e5)], "wei":list(treeArr['Weight'])[:int(2e5)]}, pick)
            else:
                pickle.dump({"label":procName, "xval":list(treeArr[variable]), "wei":list(treeArr['Weight'])}, pick)
    return procName, outName

def iPlotHist(pattern, variable, nickName='', xfunc=lambda x: [i for i in x], nbins=16,  xmin=-8, xmax=8, legend="left", cut=[],norm = True):
    processes = []
    submit_list = []
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        if "StauStau_directLLP" in root: continue
        procName = getProc(root)
        processes.append(procName)
        for i, fileName in enumerate(files):
            if 'root' not in fileName: continue
            if i>5: break
            submit_list.append((root, fileName, variable, nickName, cut))
    files = isubmit(read_histo, submit_list)
    sortedName = sorted(list(set(processes)))

    for i, process in enumerate(sortedName):
        print("Draw",process+'.')
        xbranchList, xweiList = [],[]
        for procName, filename in files:
            if procName==process:
                with open(filename,"rb") as file:
                    datadict = pickle.load(file)
                    xbranchList.extend(datadict["xval"])
                    xweiList.extend(datadict["wei"])
        print("There are ", len(xbranchList), " raw number for ", process)
        x_list, w_list = flatten(xbranchList, xweiList)
        if norm:
            x = np.array([ x for x,w in zip(x_list, w_list) if (x<=xmax) and (x>=xmin) ])
            w = np.array([ w for x,w in zip(x_list, w_list) if (x<=xmax) and (x>=xmin) ])
            w = w / w.sum()
        x = xfunc(x)
        plt.hist(x, weights=w, bins=nbins, histtype='step', range = (xmin, xmax), label=process)

    plt.title(nickName+variable)
    plt.legend(loc='upper '+legend)
    plt.savefig('TH1_'+nickName+variable+'.png',dpi=200)
    plt.close()

def PlotNewHist(pattern, branches, nickName='', function=log10, binning=16, xmin=-8, xmax=8):
    varDict = defaultdict(list)
    weiDict = defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        for fileName in files:
            if 'root' not in fileName: continue
            process = getProc(root)
            file = uproot.open(os.path.join(root, fileName))
            tree = file[treename].arrays(['Weight']+ branches)
            targetList = [function(*[evt[var] for var in branches])  for evt in tree[branches]]
            varDict[process].extend(targetList)
            weiDict[process].extend(tree['Weight'])
            if(len(weiDict[process])>1e6): break
    sortedName = sorted(varDict);
    plt.figure()
    for process in sortedName:
        x, w = flatten(varDict[process], weiDict[process])
        sumwei = w.sum()
        w = w / sumwei
        plt.hist(x, weights=w, bins=binning, histtype='step', range=(xmin, xmax), label=process)
    plt.title(nickName+'_'.join(branches))
    plt.legend(loc='upper left')
    plt.savefig('NTH1_'+nickName+'_'.join(branches)+'.png')
    plt.close()

def PlotHist2(pattern, xbranch, ybranch, xfunc, yfunc, nbinx=12, xmin=-6, xmax=6, nbiny=12, ymin=-6, ymax=6):
    xvarDict = defaultdict(list)
    yvarDict = defaultdict(list)
    weiDict  = defaultdict(list)

    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        if "StauStau_directLLP" in root: continue
        procName = getProc(root)
        for fileName in files:
            if 'root' not in fileName: continue
            file = uproot.open(os.path.join(root, fileName))
            tree = file[treename].arrays(['Weight'] + xbranch + ybranch)
            xList = [xfunc(*[evt[var] for var in xbranch])  for evt in tree[xbranch]]
            yList = [yfunc(*[evt[var] for var in ybranch])  for evt in tree[ybranch]]
            xvarDict[procName].extend(xList)
            yvarDict[procName].extend(yList)
            weiDict[procName].extend(tree['Weight'])
            if "Stau" not in root:
                if (len(xvarDict[procName]) > 1e6): 
                    break
    
    sortedName = sorted(weiDict)
    plt.figure()
    for procName in sortedName:
        x,y,w = flatten2(xvarDict[procName], yvarDict[procName], weiDict[procName])
        sumwei = w.sum()
        w = w / sumwei
        plt.hist2d(x, y, bins=(nbinx,nbiny) , weights=w, range=[[xmin, xmax], [ymin, ymax]], vmin=1e-10)
        cbar = plt.colorbar()
        cbar.cmap.set_under('white')

        plt.title(procName+"_".join(xbranch+ybranch))
        plt.xlabel("_".join(xbranch))
        plt.ylabel("_".join(ybranch))
        
        plt.savefig('TH2_'+procName+"_".join(xbranch+ybranch)+'.png')
        plt.close()

log_expr = lambda x : [log10(i) for i in x]
loge_expr = lambda x : [math.log(i) for i in x]
pt_expr = lambda x : [i/1e3 for i in x]
abs_expr = lambda x: [abs(i) for i in x]
r_expr = lambda x : [i for i in x]
#minus_expr = lambda x,y : abs(x-y)

def calc_lxy(x,y):
    lst = []
    for i in range(len(x)):
        lst.append(log10(math.sqrt(x[i]*x[i]+y[i]*y[i] + 1e-20)))
    return lst
lxy_expr = calc_lxy

def calc_lxy4(x1,y1,x2,y2):
    lst = []
    for i in range(len(x1)):
        lst.append(log10(math.sqrt((x1[i]-x2[i])**2+(y1[i]-y2[i])**2 + 1e-20)))
    return lst
lxy4_expr = calc_lxy4

def calc_IP(tauLx,tauLy,tau2Lx,tau2Ly):
    resList = []
    if len(tauLx)!=len(tau2Lx): return resList
    for i in range(len(tauLx)):
        if(tau2Ly[i]==tauLy[i] and tau2Lx[i]==tauLx[i]): continue
        sin_tau = (tau2Ly[i]-tauLy[i])/math.sqrt((tau2Ly[i]-tauLy[i])**2+(tau2Lx[i]-tauLx[i])**2)
        cos_tau = (tau2Lx[i]-tauLx[i])/math.sqrt((tau2Ly[i]-tauLy[i])**2+(tau2Lx[i]-tauLx[i])**2)
        sin_L = tau2Ly[i] / math.sqrt(tau2Ly[i]**2 + tau2Lx[i]**2)
        cos_L = tau2Lx[i] / math.sqrt(tau2Ly[i]**2 + tau2Lx[i]**2)
        ip = math.sqrt(tau2Lx[i]*tau2Lx[i] + tau2Ly[i]*tau2Ly[i] + 1e-20) * abs(sin_tau*cos_L-sin_L*cos_tau)
        logip = log10(ip)
        resList.append(logip)
    return resList
ip_expr = calc_IP

def calc_IP_trk(phi_trk,tau2Lx,tau2Ly):
    resList = []
    if 0. in phi_trk: 
        #print(phi_trk)
        return resList
    for i in range(len(tau2Lx)):
        sin_tau = math.sin(phi_trk[i])
        cos_tau = math.cos(phi_trk[i])
        sin_L = tau2Ly[i] / math.sqrt(tau2Ly[i]**2 + tau2Lx[i]**2)
        cos_L = tau2Lx[i] / math.sqrt(tau2Ly[i]**2 + tau2Lx[i]**2)
        ip = math.sqrt(tau2Lx[i]*tau2Lx[i] + tau2Ly[i]*tau2Ly[i] + 1e-20) * abs(sin_tau*cos_L-sin_L*cos_tau)
        logip = log10(ip)
        resList.append(logip)
    return resList
ip_trk_expr = calc_IP_trk

def submit(PlotHist, argLst):
    processLst=[]
    for argTuple in argLst:
        processLst.append(Process(target=PlotHist, args=argTuple))
        processLst[-1].start()
    for proc in processLst:
        proc.join()

pattern1 = ".*Test_v03.*"
pattern2 = ".*Test_v03.*StauStau_.*"
 
argLst=[
    #(".*Test_v03.*StauStau_.*", ['Matched_TruthTau_Vis_Phi', 'Matched_RecTau_Leading_Trk_phi0'], 'dPhi', minus_expr, 10,0,0.1),

    #(pattern2, ['Matched_TruthTau_Vis_Phi', 'Matched_RecTau_Leading_Trk_phi0'], 'dPhi', minus_expr, 10,0,0.1),
    #(pattern2, ['Stau_Status1_Phi', 'Truthtau_FromStauStatus1_Phi'], 'dPhi', minus_expr, 16,0,3.2),
    #(pattern2, ['TruthTau_Mother_Prod_Lx', 'TruthTau_Mother_Prod_Ly'], 'logLxy', lxy_expr, 20,-0.4,-0.3),
    #(pattern2, ['TruthTau_Mother_Decay_Lx', 'TruthTau_Mother_Decay_Ly'], 'logLxy', lxy_expr, 16,-2,6),
    #(pattern2, ['TruthTau_Prod_Lx', 'TruthTau_Prod_Ly'], 'logLxy', lxy_expr, 16,-2,6),
    #(pattern2, ['TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly'], 'logLxy', lxy_expr, 16,-2,6),
    #(pattern2, ['TruthTau_Prod_Lx', 'TruthTau_Prod_Ly', 'TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly'], 'logLxy', lxy4_expr, 16,-8,8),
    (pattern2, ['TruthTau_Prod_Lx', 'TruthTau_Prod_Ly', 'TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly'], 'logIP', ip_expr, 16,-8,8),
    (pattern2, ['TruthTau_LeadingTrk_phi', 'TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly'], 'logIP', ip_trk_expr, 16,-8,8),
]
#submit(PlotNewHist, argLst)

def dsid_transfer(dsid):
    if abs(dsid)==1000015 or abs(dsid)==2000015:
        return 0 # stau
    elif abs(dsid)==1000022:
        return 1 #N1
    elif abs(dsid)<1000: 
        return 2
    else:
        return 3
dsid_expr = lambda x : [dsid_transfer(i) for i in x]

#pattern0 = ".*Testn03_.*Stau.*"
pattern0 = ".*Testn03_.*"
argLst=[

    #(".*Testn05_.*Stau.*", 'TruthTau_LeadingTrk_d0', 'v1', log_expr, 20,-6,4, 'left', ['TruthTau_Mother_PDGID',dsid_transfer,-1,1]),
    #(".*Testn05_.*Stau.*", 'TruthTau_LeadingTrk_d0', 'v2', log_expr, 20,-6,4, 'left', ['TruthTau_Mother_PDGID',dsid_transfer,0,2]),
    #(".*Testn05_.*Stau.*", 'TruthTau_LeadingTrk_d0', 'v3', log_expr, 20,-6,4, 'left', ['TruthTau_Mother_PDGID',dsid_transfer,1,3]),
    #(".*Testn05_.*Stau.*", 'TruthTau_LeadingTrk_d0', 'v4', log_expr, 20,-6,4, 'left', ['TruthTau_Mother_PDGID',dsid_transfer,2,4]),

    #(".*Testn05_.*Stau.*", 'TruthTau_Lxy', 'v1', log_expr, 14,-3,4, 'left', ['TruthTau_LastSUSY_Mother_PDGID',dsid_transfer,-1,1]),
    #(".*Testn05_.*Stau.*", 'TruthTau_Lxy', 'v2', log_expr, 14,-3,4, 'left', ['TruthTau_LastSUSY_Mother_PDGID',dsid_transfer, 0,2]),
    #(".*Testn05_.*Stau.*", 'TruthTau_Lxy', 'v3', log_expr, 14,-3,4, 'left', ['TruthTau_LastSUSY_Mother_PDGID',dsid_transfer, 1,3]),
    #(".*Testn05_.*Stau.*", 'TruthTau_Lxy', 'v4', log_expr, 14,-3,4, 'left', ['TruthTau_LastSUSY_Mother_PDGID',dsid_transfer, 2,4]),

    #(".*Testn03_.*", 'TruthTau_Vis_Pt' 'v02', pt_expr,20,0,200),
    #(".*Testn03_.*", 'Matched_TruthTau_Vis_Eta', 'v02', abs_expr,14,-0.2,2.6),
    #(".*Testn03_.*", 'TruthTau_Vis_Eta', 'v02', abs_expr, 48, -4, 20),

    ##(".*Testn03_.*Py8_.*", 'Matched_TruthTau_Num', 'v01', abs_expr, 4, 0, 4, 'right'),
    ##(".*Testn03_.*Stau.*", 'Matched_TruthTau_Num', 'v02', abs_expr, 4, 0, 4, 'right'),
    ##(".*Testn03_.*Py8_.*", 'TruthTau_Num', 'v01', abs_expr, 7, 0, 7, 'right'),
    ##(".*Testn03_.*Stau.*", 'TruthTau_Num', 'v02', abs_expr, 7, 0, 7, 'right'),

    #(".*Testn03_.*", 'Matched_TruthTau_Num', 'v00', abs_expr, 4, 0, 4, 'right'),
    #(".*Testn03_.*", 'TruthTau_Num', 'v00', abs_expr, 7, 0, 7, 'right'),
    #(".*Testn03_.*", 'RecTau_Num', 'v02', abs_expr, 20, 0, 20, 'right'),
    #(".*Testn03_.*", 'RecTau_ID', 'v02', abs_expr, 4, 0, 4, 'left'),
    #(".*Testn03_.*", 'RecTau_ID', 'v15', abs_expr, 4, 0, 4, 'right', ['TruthTau_Mother_PDGID',abs,1000014,1000016]),
    #(".*Testn03_.*", 'RecTau_ID', 'v25', abs_expr, 4, 0, 4, 'right', ['TruthTau_Mother_PDGID',abs,2000014,2000016]),
    #(".*Testn03_.*", 'RecTau_ID', 'v22', abs_expr, 4, 0, 4, 'right', ['TruthTau_Mother_PDGID',abs,1000021,1000023]),

    #(pattern0, 'Matched_RecTau_Leading_Trk_d0', 'v01', log_expr,32,-5,3),
    #(pattern0, 'Matched_TruthTau_LeadingTrk_d0', 'v01', log_expr,32,-5,3),
    #(pattern0, 'TruthTau_d0', 'v01', log_expr,32,-5,3),
    #(pattern0, 'Matched_TruthTau_d0', 'v01', log_expr,32,-5,3),
    #(".*Testn03_.*Stau.*", 'TruthTau_LeadingTrk_d0', 'v01', log_expr,32,-5,3),

    #(".*Testn03_.*Stau_200.*", 'TruthTau_Lxy', 'v02', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_200.*", 'Matched_TruthTau_Lxy', 'v02', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_300.*", 'TruthTau_Lxy', 'v03', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_300.*", 'Matched_TruthTau_Lxy', 'v03', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_400.*", 'TruthTau_Lxy', 'v04', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_400.*", 'Matched_TruthTau_Lxy', 'v04', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_500.*", 'TruthTau_Lxy', 'v05', log_expr,16,-2,6),
    #(".*Testn03_.*Stau_500.*", 'Matched_TruthTau_Lxy', 'v05', log_expr,16,-2,6),
    #(pattern0, 'Stau_Decay_Lxy', 'v01', log_expr,16,-2,6),
    #(pattern0, 'Stau_Prod_Lxy', 'v01', log_expr,20,-0.4,-0.3),
    #(pattern0, 'Matched_TruthTau_Vis_Pt', 'v01', pt_expr,40,0,400),

    #(".*Testn05_.*", 'TruthTau_Min_dR', 'v3', r_expr, 30,0,6, 'right',['TruthTau_Min_dR',abs,0,6]), 
    #(".*Testn05_.*", 'TruthTau_Min_dR', 'v2', r_expr, 30,0,6, 'right'), 
]
print("0aaaa")
submit(iPlotHist, argLst)

argLst=[
    #(pattern2, ['TruthTau_Lxy'],['TruthTau_d0'], log_expr, log_expr),
    #(pattern2, ['TruthTau_d0'],['TruthTau_Lxy'], log_expr, log_expr),
    #(pattern2, ['TruthTau_Lxy'],['TruthTau_Vis_Pt'], log_expr, pt_expr,12,-6,6,20,0,1000),
    #(".*Testn03_.*Stau.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_Lxy'], log_expr, log_expr,12,-4,2,8,-1,3),
    #(".*Testn03_.*Ztautau.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_Lxy'], log_expr, log_expr,12,-4,2,8,-1,3),
    #(".*Testn03_.*ttbar.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_Lxy'], log_expr, log_expr,12,-4,2,8,-1,3),
    (".*Testn03_.*Stau.*", ['TruthTau_Num'], ['Matched_TruthTau_Num'], abs, abs, 7,0,7,4,0,4),

    #(".*Testn03_.*Stau.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_R'], log_expr, r_expr,12,-6,6,10,0,1),
    #(".*Testn03_.*Ztautau.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_R'], log_expr, r_expr,8,-6,2,10,0,1),
    #(".*Testn03_.*ttbar.*", ['TruthTau_LeadingTrk_d0'],['TruthTau_R'], log_expr, r_expr,8,-6,2,10,0,1),
    #(pattern2, ['TruthTau_Prod_Lx', 'TruthTau_Prod_Ly'], ['TruthTau_d0'], lxy_expr, log_expr),
    #(pattern2, ['TruthTau_LeadingTrk_Prod_Lx', 'TruthTau_LeadingTrk_Prod_Ly'], ['TruthTau_d0'], lxy_expr, log_expr),
    #(pattern2, ['Matched_TruthTau_Prod_Lxy'], ['Matched_RecTau_Leading_Trk_d0'], log_expr, log_expr),
    #(pattern2, ['Matched_TruthTau_Lxy'], ['Matched_RecTau_Leading_Trk_d0'], log_expr, log_expr),
    #(pattern2, ['Matched_TruthTau_Lxy', 'Matched_TruthTau_Lz'], ['Matched_RecTau_Leading_Trk_d0', 'Matched_RecTau_Leading_Trk_z0sintheta'], lxy_expr, lxy_expr),
]
#print("aaaaa")
#submit(PlotHist2, argLst)
