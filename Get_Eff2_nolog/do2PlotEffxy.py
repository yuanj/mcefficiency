import json
import numpy as np
import ROOT
import pickle
from array import array
from multiprocessing import Process
import re
# draw nom or denomwith rawnumber to check statistics
# draw efficiency plot
# create efficiency workspace

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+6, 
        ROOT.kYellow, ROOT.kRed
]
ROOT.gROOT.SetBatch(True)


def tobinning(x):
    return [len(x)-1,x]

def drawEff(jfile, xbins, ybins, checkEFF=False, saveROOT=False):
    if saveROOT: checkEFF=True
    # check statistics
    xbinning = tobinning(xbins)
    ybinning = tobinning(ybins)

    matched = re.match("effxy_TruthTau_leading_(.*)_TruthTau_leading_(.*)_(.p.*).json",jfile)
    xtitle, ytitle, tag = matched.groups()
    if saveROOT: root_file = ROOT.TFile(jfile.split('.')[0]+".root", "RECREATE")
    with open(jfile) as f:
        data = json.load(f)
        sortedName = sorted(list(data.keys()))

        for i, process in enumerate(sortedName):
            if process=="ttbar_allhad": continue

            xlist, ylist, effList = [],[],[]
            if checkEFF: wlist = []
            for filename in data[process]:
                with open(filename,"rb") as file:
                    datadict = pickle.load(file)
                    xlist.extend(datadict["xval"])
                    ylist.extend(datadict["yval"])
                    effList.extend(datadict["eff"])
                    if checkEFF: wlist.extend(datadict["wei"])

            arg_h1 = [process,process]+xbinning+ybinning
            arg_h2 = [process+"down",process+"down"]+xbinning+ybinning
            hist = ROOT.TH2F(*arg_h1)
            hist_down = ROOT.TH2F(*arg_h2)
            
            if checkEFF:
                for x,y,e,w in zip(xlist, ylist, effList,wlist):
                    if e>0: 
                        hist.Fill(x,y,w)
                    hist_down.Fill(x,y,w)
            else:
                for x,y,e in zip(xlist, ylist, effList):
                    if e>0: 
                        hist.Fill(x,y)
                    hist_down.Fill(x,y)
            if checkEFF:
                if saveROOT:
                    hist.Divide(hist_down)
                    hist.Write()
                else:
                    canvas = ROOT.TCanvas("", "", 1600, 1200)
                    hist.Divide(hist_down)
                    hist.SetBarOffset(0.2)
                    hist.SetStats(0)
                    hist.Draw("COLZ TEXT")
                    hist.SetXTitle(xtitle)
                    hist.SetYTitle(ytitle)

                    arg_hn = [process+"new",process+"new"]+xbinning+ybinning
                    histn = ROOT.TH2F(*arg_hn)
                    for i in range(hist.GetNbinsX()):
                        for j in range(hist.GetNbinsY()):
                            if hist.GetBinContent(i+1,j+1)>0:
                                histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1))
                                #histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1)/hist.GetBinContent(i+1,j+1))
                            else:
                                histn.SetBinContent(i+1,j+1,0)
                            #histn.SetBinContent(i+1,j+1,hist.GetBinError(i+1,j+1))
                    histn.SetMarkerColor(ROOT.kRed)
                    histn.SetBarOffset(-0.2)
                    histn.SetStats(0)
                    ROOT.gStyle.SetPaintTextFormat("4.3f");
                    histn.Draw("TEXT SAME")
                    hist.Draw("TEXT SAME")
                    hist.GetZaxis().SetRangeUser(0., 1.);

                    latex = ROOT.TLatex()
                    latex.SetNDC ()
                    latex.SetTextSize (0.06)
                    latex.DrawText (0.7 ,0.95 ,tag[0:2])
                    if tag[-1] == "f" or tag[-1] == "c":
                        latex.SetTextSize (0.06)
                        latex.DrawText (0.76 ,0.95 , "forward" if tag[-1] == "f" else "central")

                    canvas.SaveAs("EFF2_"+process+"_"+xtitle+"_"+ytitle+"_"+tag+".png")
            else:
                canvas = ROOT.TCanvas("", "", 1600, 1200)
                hist.Draw("BOX text")
                hist.SetXTitle(xtitle)
                hist.SetYTitle(ytitle)
                canvas.SaveAs("RAW_"+process+"_"+xtitle+"_"+ytitle+"_"+tag+".png")

                #canvas2 = ROOT.TCanvas("", "", 1600, 1200)
                #hist_down.Draw("BOX text")
                #hist_down.SetXTitle(xtitle)
                #hist_down.SetYTitle(ytitle)
                #canvas2.SaveAs("RAWtruth_"+process+"_"+xtitle+"_"+ytitle+"_"+tag+".png")
    if saveROOT: root_file.Close()
#lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
#lin1 = array('f', [-3, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0])

#din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
#din2 = array('f', [-6,]+[x/2. for x in range(-3,3,1)]+[4,])
#din3 = array('f', [x/2. for x in range(-3,3,1)])

#ptbin = array('f', [40,60,80,100,120,160,200])
#ein1 = array('f', [x/10. for x in range(-30,31,5)])
#ein2 = array('f', [-3,-1.52,-1.37,1.37,1.52,3])

#din2 = array('f', [-6,-0.5,0,0.5,1,2,4])
#lin1 = array('f', [-3,1,1.5,2,2.5,3,4])
#lin1 = array('f', [-3,0,1,1.5,2,2.5,3,4]) # consider 0

din1 = array('f', [x for x in range(0,10,1)])
lin1 = array('f', [x for x in range(0,105,5)])

arglist = [
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_stau_3p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_3p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_1p.json', din1, lin1],
    ['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_sm_3p.json', din1, lin1],
    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_1p.json', din1, lin1],
    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n1_3p.json', din1, lin1],
    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n2_1p.json', din1, lin1],
    #['effxy_TruthTau_leading_LeadingTrk_d0_TruthTau_leading_Lxy_n2_3p.json', din1, lin1],
]

processLst = []
for pargs in arglist:
    #processLst.append(Process(target=drawEff, args=(pargs)))
    #processLst.append(Process(target=drawEff, args=(pargs+[True,True])))
    processLst.append(Process(target=drawEff, args=(pargs+[True,])))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
