import ROOT
import re
import pickle
import os
from doPlotEffxy import getProc,getName
import math

def getX(x, name):
    if "Lxy" in name:
        return  x
    elif "d0" in name:
        return x
    elif "Pt" in name:
        return x/1e3
    elif "Eta" in name:
        return abs(x)
    elif "absPDGID" in name:
        if x ==1000022:
            return 1
        elif x==1000015 or x==2000015:
            return 2
        elif x==1000023 or x==1000024:
            return 3
        else:
            return 0
    else:
        return x

def calc_eff(num, root, ifile, xbranch, ybranch, match_branch, ptcut, ptcut2, nprong, cut_branch, clow, chigh):
    procName = getProc(root)
    outname = getName(num, procName, xbranch, ybranch, match_branch, ptcut, ptcut2, nprong, cut_branch, clow, chigh)

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, yArray, effArray, weiArray = [],[],[],[]
    for event in file.analysis:
        ptpass = getattr(event, 'TruthTau_leading_Pt') > ptcut and getattr(event, 'TruthTau_leading_Pt') < ptcut2
        np_pass = (nprong<0) or (getattr(event, 'TruthTau_leading_ChargedPion') == nprong)
        etapass = abs(getattr(event, 'TruthTau_leading_Eta'))<1.37 or (abs(getattr(event, 'TruthTau_leading_Eta'))>1.52 and abs(getattr(event, 'TruthTau_leading_Eta'))<2.5)
        npbasepass = (getattr(event, 'TruthTau_leading_ChargedPion') == 1) or (getattr(event, 'TruthTau_leading_ChargedPion') == 3)
        #print ptpass, np_pass, etapass, npbasepass
        if cut_branch!="no":
            val = getattr(event, cut_branch)
            passcut = (getX(val,cut_branch)>clow and getX(val,cut_branch)<chigh) and ptpass and np_pass and etapass and npbasepass
        else:
            passcut = ptpass and np_pass and etapass and npbasepass
        if passcut:
            #print getX(getattr(event, xbranch), xbranch), getX(getattr(event, ybranch), ybranch)
            xArray.append(getX(getattr(event, xbranch), xbranch))
            yArray.append(getX(getattr(event, ybranch), ybranch))
            effArray.append(getattr(event, match_branch))
            weiArray.append(getattr(event,'Weight'))
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"yval":yArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname

import argparse
parser = argparse.ArgumentParser(description='Calculate efficiency')
parser.add_argument('num', type=int, help='unique number for output file')
parser.add_argument('root', type=str, help='Root directory')
parser.add_argument('ifile', type=str, help='Input file')
parser.add_argument('xbranch', type=str, help='X Truth branch name')
parser.add_argument('ybranch', type=str, help='Y Truth branch name')
parser.add_argument('match_branch', type=str, help='Match branch name')
parser.add_argument('ptcut', type=float, help='PT cut value')
parser.add_argument('ptcut2', type=float, help='PT cut value')
parser.add_argument('nprong', type=float, help='nprong value')
parser.add_argument('cut_branch', type=str, help='Cut branch name')
parser.add_argument('clow', type=float, help='CLow value')
parser.add_argument('chigh', type=float, help='CHigh value')
args = parser.parse_args()

calc_eff(args.num, args.root, args.ifile, args.xbranch, args.ybranch, args.match_branch, args.ptcut, args.ptcut2, args.nprong, args.cut_branch, args.clow, args.chigh)
