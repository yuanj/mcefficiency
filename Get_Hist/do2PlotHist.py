import json
import numpy as np
import pickle
import ROOT
from array import array
from multiprocessing import Process

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+7, 
        ROOT.kMagenta, 
        ROOT.kCyan, ROOT.kSpring, ROOT.kYellow+1, ROOT.kRed+2
]
ROOT.gROOT.SetBatch(True)

def tobinning(x):
    return [len(x)-1,x]

def drawPlot(jfile, bins):
    binning = tobinning(bins)
    PlotTitle = 'log10_'+jfile.split('.')[0] if 'd0' in jfile or 'Lxy' in jfile else jfile.split('.')[0]
    with open(jfile) as f:
        data = json.load(f)
        sortedName = sorted(list(data.keys()))

        canvas = ROOT.TCanvas("", "", 1600, 1200)
        isFirst = True
        legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)
        TTstore=[]

        for i, process in enumerate(sortedName):
            branchList, weiList = [],[]
            for filename in data[process]:
                with open(filename,"rb") as pfile:
                    datadict = pickle.load(pfile)
                    branchList.extend(datadict["xval"])
                    weiList.extend(datadict["wei"])
            weiList = np.array(weiList)
            weiList = weiList / weiList.sum()

            arg_h1 = [process,process]+binning
            hist = ROOT.TH1F(*arg_h1)
            TTstore.append(hist)
            for x,w in zip(branchList, weiList):
                hist.Fill(x,w)
            hist.SetLineColor(UserColors[i])
            hist.SetLineWidth(3)
            if isFirst:
                hist.Draw('HIST')
                hist.SetXTitle(PlotTitle)
                # hist.SetYTitle("efficiency")
                hist.SetTitle('')
                hist.GetYaxis().SetRangeUser(0., .5);
                isFirst = False
            else:
                hist.Draw('HISTSAME')
            hist.SetStats(0)
            legend.AddEntry(hist, process, "l")
        legend.Draw()
        canvas.SaveAs("HIST1_"+jfile.split('.')[0]+".png")

lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
lin1 = array('f', [-3,-0.5]+[x/4. for x in range(0,17,1)])
din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
din2 = array('f', [-6,-1.5]+[x/2. for x in range(-2,9,1)])
din1 = array('f', [-6,-3]+[x/4. for x in range(-10,17,1)])
ptbin = array('f', list(range(40,210,10)))
ein1 = array('f', [x/10. for x in range(-30,31,5)])
ein2 = array('f', [-3,-1.52,-1.37,1.37,1.52,3])

arglist=[
    ['hist_TruthTau_leading_Eta_m1.json', ein2],
    ['hist_TruthTau_leading_Eta_m2.json', ein2],
    ['hist_TruthTau_leading_Eta_p1.json', ein2],
    ['hist_TruthTau_leading_Eta_t1.json', ein2],
    ['hist_TruthTau_leading_Eta_t2.json', ein2],

    ['hist_TruthTau_leading_LeadingTrk_d0_m1.json', din1],
    ['hist_TruthTau_leading_LeadingTrk_d0_m2.json', din1],
    ['hist_TruthTau_leading_LeadingTrk_d0_p1.json', din1],
    ['hist_TruthTau_leading_LeadingTrk_d0_t1.json', din1],
    ['hist_TruthTau_leading_LeadingTrk_d0_t2.json', din1],

    ['hist_TruthTau_leading_Lxy_m1.json', lin1],
    ['hist_TruthTau_leading_Lxy_m2.json', lin1],
    ['hist_TruthTau_leading_Lxy_p1.json', lin1],
    ['hist_TruthTau_leading_Lxy_t1.json', lin1],
    ['hist_TruthTau_leading_Lxy_t2.json', lin1],

    ['hist_TruthTau_leading_Pt_m1.json', ptbin],
    ['hist_TruthTau_leading_Pt_m2.json', ptbin],
    ['hist_TruthTau_leading_Pt_p1.json', ptbin],
    ['hist_TruthTau_leading_Pt_t1.json', ptbin],
    ['hist_TruthTau_leading_Pt_t2.json', ptbin],
]
processLst = []
for pargs in arglist:
    processLst.append(Process(target=drawPlot, args=(pargs)))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
