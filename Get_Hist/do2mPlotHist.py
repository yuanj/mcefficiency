import json
import numpy as np
import pickle
import ROOT
from array import array
from multiprocessing import Process
import re

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kViolet,
        ROOT.kTeal,  ROOT.kPink+6, 
        ROOT.kYellow, ROOT.kRed
]
ROOT.gROOT.SetBatch(True)

def tobinning(x): return [len(x)-1,x]

def drawPlot(name, jfile_list, pattern_list, bins):
    if not isinstance(jfile_list, list):
        jfile_list = [jfile_list]
    if not isinstance(pattern_list, list):
        pattern_list = [pattern_list]

    binning = tobinning(bins)

    jfile=jfile_list[0]
    PlotTitle = jfile_list[0].split('.')[0]
    if 'd0' in PlotTitle or 'Lxy' in PlotTitle:
        PlotTitle = 'log10_'+PlotTitle

    data = {}
    for jfile in jfile_list:
        with open(jfile,'r') as f:
            file_data = json.load(f)
            data.update(file_data)

    sortedName = sorted(list(data.keys()))
    canvas = ROOT.TCanvas("", "", 1600, 1200)
    isFirst = True
    legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)
    TTstore=[]
    
    i = 0
    for process in sortedName:
        if process=="ttbar_allhad": continue
        if all(re.match(pattern, process) == None for pattern in pattern_list): 
            continue
        #print process
        branchList, weiList = [],[]
        for filename in data[process]:
            with open(filename,"rb") as pfile:
                datadict = pickle.load(pfile)
                branchList.extend(datadict["xval"])
                weiList.extend(datadict["wei"])
        weiList = np.array(weiList)
        weiList = weiList / weiList.sum()

        arg_h1 = [process,process]+binning
        hist = ROOT.TH1F(*arg_h1)
        TTstore.append(hist)
        for x,w in zip(branchList, weiList):
            hist.Fill(x,w)
        hist.SetLineColor(UserColors[i])
        i+=1
        hist.SetLineWidth(3)
        if isFirst:
            hist.Draw('HIST')
            hist.SetXTitle(PlotTitle)
            # hist.SetYTitle("efficiency")
            hist.SetTitle('')
            hist.GetYaxis().SetRangeUser(0., .5);
            hist.GetYaxis().SetRangeUser(0., .3);
            isFirst = False
        else:
            hist.Draw('HISTSAME')
        hist.SetStats(0)
        legend.AddEntry(hist, process, "l")
    legend.Draw()
    canvas.SaveAs("HIST1_"+name+".png")

lin1 = array('f', [-3,-0.5]+[x/2. for x in range(-1,9,1)])
din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
din1 = array('f', [-6,-5,]+[x/4. for x in range(-16,9,1)])
ptbin = array('f', list(range(40,310,20)))
#ein1 = array('f', [x/10. for x in range(-30,31,5)])
ein2 = array('f', [-3,-1.52,-1.37,0,1.37,1.52,3])


arglist=[
        # a great config
#        ## same kinematics for different particles for p/l, don't use them together
#        ("eta_st_100",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_100_133_.*",ein2),
#        ("eta_st_200",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_200_133_.*",ein2),
#        ("eta_st_300",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_300_133_.*",ein2),
#        ("eta_st_400",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_400_133_.*",ein2),
#        ("eta_st_500",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_500_133_.*",ein2),
#
#        ("eta_n1_100",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_100_133_.*",ein2),
#        ("eta_n1_200",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_200_133_.*",ein2),
#        ("eta_n1_300",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_300_133_.*",ein2),
#        ("eta_n1_400",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_400_133_.*",ein2),
#        ("eta_n1_500",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_500_133_.*",ein2),
#
#        ("eta_st_0",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_1ns",ein2),
#        ("eta_st_1",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p1ns",ein2),
#        ("eta_st_2",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p01ns",ein2),
#        ("eta_st_3",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p001ns",ein2),
#        ("eta_st_4",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p0001ns",ein2),
#        ("eta_st_p",["hist_TruthTau_leading_Eta_1p_stau.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_prompt",ein2),
#                    
#        ("eta_n1_0",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_1ns",ein2),
#        ("eta_n1_1",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p1ns",ein2),
#        ("eta_n1_2",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p01ns",ein2),
#        ("eta_n1_3",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p001ns",ein2),
#        ("eta_n1_4",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_0p0001ns",ein2),
#        ("eta_n1_p",["hist_TruthTau_leading_Eta_1p_n1.json","hist_TruthTau_leading_Eta_1p_sm.json"], r"stau_.00_133_prompt",ein2),

        ("d0_st_100",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_100_133_.*",din1),
        ("d0_st_200",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_200_133_.*",din1),
        ("d0_st_300",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_300_133_.*",din1),
        ("d0_st_400",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_400_133_.*",din1),
        ("d0_st_500",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_500_133_.*",din1),

        ("d0_n1_100",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_100_133_.*",din1),
        ("d0_n1_200",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_200_133_.*",din1),
        ("d0_n1_300",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_300_133_.*",din1),
        ("d0_n1_400",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_400_133_.*",din1),
        ("d0_n1_500",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_500_133_.*",din1),

        ("d0_st_0",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_1ns",din1),
        ("d0_st_1",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p1ns",din1),
        ("d0_st_2",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p01ns",din1),
        ("d0_st_3",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p001ns",din1),
        ("d0_st_4",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p0001ns",din1),
        ("d0_st_p",["hist_TruthTau_leading_LeadingTrk_d0_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_prompt",din1),

        ("d0_n1_0",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_1ns",din1),
        ("d0_n1_1",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p1ns",din1),
        ("d0_n1_2",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p01ns",din1),
        ("d0_n1_3",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p001ns",din1),
        ("d0_n1_4",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p0001ns",din1),
        ("d0_n1_p",["hist_TruthTau_leading_LeadingTrk_d0_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_prompt",din1),

        ("lxy_st_100",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_100_133_.*", lin1),
        ("lxy_st_200",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_200_133_.*", lin1),
        ("lxy_st_300",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_300_133_.*", lin1),
        ("lxy_st_400",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_400_133_.*", lin1),
        ("lxy_st_500",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_500_133_.*", lin1),

        ("lxy_n1_100",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_100_133_.*", lin1),
        ("lxy_n1_200",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_200_133_.*", lin1),
        ("lxy_n1_300",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_300_133_.*", lin1),
        ("lxy_n1_400",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_400_133_.*", lin1),
        ("lxy_n1_500",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_500_133_.*", lin1),

        ("lxy_st_0",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_1ns",lin1),
        ("lxy_st_1",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p1ns",lin1),
        ("lxy_st_2",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p01ns",lin1),
        ("lxy_st_3",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p001ns",lin1),
        ("lxy_st_4",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p0001ns",lin1),
        ("lxy_st_p",["hist_TruthTau_leading_Lxy_1p_stau.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_prompt",lin1),

        ("lxy_n1_0",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_1ns",lin1),
        ("lxy_n1_1",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p1ns",lin1),
        ("lxy_n1_2",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p01ns",lin1),
        ("lxy_n1_3",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p001ns",lin1),
        ("lxy_n1_4",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_0p0001ns",lin1),
        ("lxy_n1_p",["hist_TruthTau_leading_Lxy_1p_n1.json","hist_TruthTau_leading_Lxy_1p_sm.json"], r"stau_.00_133_prompt",lin1),

        ("Pt_st_100",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_100_133_.*", ptbin),
        ("Pt_st_200",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_200_133_.*", ptbin),
        ("Pt_st_300",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_300_133_.*", ptbin),
        ("Pt_st_400",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_400_133_.*", ptbin),
        ("Pt_st_500",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_500_133_.*", ptbin),

        ("Pt_n1_100",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_100_133_.*", ptbin),
        ("Pt_n1_200",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_200_133_.*", ptbin),
        ("Pt_n1_300",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_300_133_.*", ptbin),
        ("Pt_n1_400",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_400_133_.*", ptbin),
        ("Pt_n1_500",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_Pt_1p_sm.json"], r"stau_500_133_.*", ptbin),

        ("Pt_st_0",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_1ns", ptbin),
        ("Pt_st_1",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p1ns", ptbin),
        ("Pt_st_2",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p01ns", ptbin),
        ("Pt_st_3",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p001ns", ptbin),
        ("Pt_st_4",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p0001ns", ptbin),
        ("Pt_st_p",["hist_TruthTau_leading_Pt_1p_stau.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_prompt", ptbin),

        ("Pt_n1_0",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_1ns", ptbin),
        ("Pt_n1_1",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p1ns", ptbin),
        ("Pt_n1_2",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p01ns", ptbin),
        ("Pt_n1_3",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p001ns", ptbin),
        ("Pt_n1_4",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_0p0001ns", ptbin),
        ("Pt_n1_p",["hist_TruthTau_leading_Pt_1p_n1.json","hist_TruthTau_leading_LeadingTrk_d0_1p_sm.json"], r"stau_.00_133_prompt", ptbin),
]
#arglist=[
#        ("300_0p01_1p",["hist_TruthTau_leading_LeadingTrk_d0_p1.json","hist_TruthTau_leading_LeadingTrk_d0_l1.json","hist_TruthTau_leading_LeadingTrk_d0_z1.json"], [r".*_300_133_0p01ns",r".*_300_133_prompt",r"Ztautau",r"ttbar"],din2),
#        ("300_0p01_3p",["hist_TruthTau_leading_LeadingTrk_d0_p3.json","hist_TruthTau_leading_LeadingTrk_d0_l3.json","hist_TruthTau_leading_LeadingTrk_d0_z3.json"], [r".*_300_133_0p01ns",r".*_300_133_prompt",r"Ztautau",r"ttbar"],din2),
#]
processLst = []
for pargs in arglist:
    processLst.append(Process(target=drawPlot, args=(pargs)))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
