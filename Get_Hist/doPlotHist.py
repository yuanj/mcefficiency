# import uproot
import math
# import matplotlib.pyplot as plt
from collections import defaultdict
import os
import re
import numpy as np
import pickle
from multiprocessing import Process, Queue
import time
import json

def getProc(fullName):
    folder = fullName.split('/')[-1]
    if "StauStau_directLLP" in fullName:
        return "gmsb_"+re.findall(r".*StauStau_directLLP_.00_0_(.*ns).*", folder)[0]
    elif "Stau_" in fullName:
        matched = re.match(r".*Stau_(.*0)_50_(.33)_(.*ns).*", folder)
        return "stau_"+matched.groups()[0]+"_"+matched.groups()[1]+"_"+matched.groups()[2]
    elif "RPCRPV" in fullName:
        matched = re.match(r".*StauRPCRPV_(.*0)_50_(.33).*", folder)
        return "stau_"+matched.groups()[0]+"_"+matched.groups()[1]+"_prompt"
    elif "N1N2" in fullName:
        matched = re.match(r".*Testn09_E\..*N1N2_.*_(.*)_(.*)_(.*ns).*", folder) 
        return "N1N2_"+matched.groups()[0]+"_"+matched.groups()[1]+"_"+matched.groups()[2]
    elif "Ztautau" in fullName:
        return re.findall(r".*Py8_(.*)_.*", folder)[0]
    elif "ttbar" in fullName:
        return re.findall(r".*Py8_(ttbar_.*)_.*", folder)[0]
    else:
        raise Exception("Sorry, the process isn't matched.")

def getName(num, procName, variable, ptcut, ptcut2, nprong, cut_branch, clow, chigh):
    return "hist_"+str(num)+"_"+procName+"_"+variable+"_tau"+str(int(nprong))+"p"+str(ptcut/1e3)+str(ptcut2/1e3)+"_"+cut_branch+"_"+str(float(clow))+"_"+str(float(chigh))+".pk"

def find_max_number(directory):
    max_number = -1
    for filename in os.listdir(directory):
        full_path = os.path.join(directory, filename)
        if re.match(r'hist.sub\.sh\.\d+', filename):
            number = int(re.search(r'\d+$', filename).group())
            max_number = max(max_number, number)
    return max_number

def run(func, argList):
    out = []
    for args in argList:
        out.append(func(*args))
    return out

def PlotHist(pattern, variable, nickName='', ptcut=40e3, cutbranch='', cutlow='', cuthigh=''):
    treename = 'analysis'
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2/'

    outputs=defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        procName = getProc(root)
        for i, file in enumerate(files):
            if 'Stau' not in procName and i>1:  break
            num = str(find_max_number('.')+1)
            with open('hist_sub.sh.'+num,'w') as f:
                f.write('python real_job_hist.py '+' '.join([num, root, file, variable, str(ptcut), cutbranch, str(cutlow), str(cuthigh)]))
            os.system('chmod u+x hist_sub.sh.'+num)
            output = getName(num, procName, variable, ptcut, cutbranch, cutlow, cuthigh)
            outputs[procName].append(output)
    
    with open('hist_'+variable+'_'+nickName+".json", "w") as outfile:
        json.dump(outputs, outfile)
    return 'hist_'+variable+'_'+nickName+".json"

def leadingPlotHist(pattern, variable, nickName='', ptcut=40e3, ptcut2=1e6, nprong=-1, cutbranch='', cutlow='', cuthigh=''):
    treename = 'analysis'
    sourceDir = '/publicfs/atlas/atlasnew/SUSY/users/yuanjiarong/RPV/mcmc2/Ntuples2/'

    outputs=defaultdict(list)
    for root, dirs, files in os.walk(sourceDir):
        if not re.search(pattern, root.split('/')[-1]): continue
        procName = getProc(root)
        ind=0
        for file in files:
            num = str(find_max_number('.')+1)
            with open('hist_sub.sh.'+num,'w') as f:
                f.write('python real_job_hist_leading.py '+' '.join([num, root, file, variable, str(ptcut), str(ptcut2), str(nprong), cutbranch, str(cutlow), str(cuthigh)]))
            os.system('chmod u+x hist_sub.sh.'+num)
            output = getName(num, procName, variable, ptcut, ptcut2, nprong, cutbranch, cutlow, cuthigh)
            outputs[procName].append(output)
            if "Ztautau" in procName or "ttbar" in procName: 
                ind+=1
                if ind>2: break
    
    with open('hist_'+variable+'_'+nickName+".json", "w") as outfile:
        json.dump(outputs, outfile)

    return 'hist_'+variable+'_'+nickName+".json"

if __name__ == '__main__':
    os.system('rm -f hist_sub.sh.*')
    #os.system('rm -f hist_*.json')
    argLIST2= [
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_Lxy", 'z', 40e3, 1e6, "TruthTau_leading_Lxy", -10, 10),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_Pt", 'z', 40e3, 1e6, "TruthTau_leading_Pt", 0, 2e9),
        #(".*n09_E.(Py8_ttbar|PoPy8_Ztautau).*", "TruthTau_leading_Eta", 'z', 40e3, 1e6, "TruthTau_leading_Pt", 0, 2e9),

        #(".*n09.*Stau.*", "TruthTau_leading_Lxy", 'l', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        #(".*n09.*Stau.*", "TruthTau_leading_Lxy", 'p', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),

        #(".*n09.*Stau.*", "TruthTau_leading_Pt", 'l', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        #(".*n09.*Stau.*", "TruthTau_leading_Pt", 'p', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),

        #(".*n09.*Stau.*", "TruthTau_leading_Eta", 'l', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        #(".*n09.*Stau.*", "TruthTau_leading_Eta", 'p', 40e3, 1e6, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),
        
        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", '1p_sm', 40e3, 1e6, 1, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_LeadingTrk_d0", '3p_sm', 40e3, 1e6, 3, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09.*Stau.*", "TruthTau_leading_LeadingTrk_d0", '1p_n1', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_LeadingTrk_d0", '3p_n1', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_LeadingTrk_d0", '1p_stau', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),
        (".*n09.*Stau.*", "TruthTau_leading_LeadingTrk_d0", '3p_stau', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),

        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_Pt", '1p_sm', 40e3, 1e6, 1, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_Pt", '3p_sm', 40e3, 1e6, 3, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09.*Stau.*", "TruthTau_leading_Pt", '1p_n1', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_Pt", '3p_n1', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_Pt", '1p_stau', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),
        (".*n09.*Stau.*", "TruthTau_leading_Pt", '3p_stau', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),

        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_Lxy", '1p_sm', 40e3, 1e6, 1, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09_E.(Py8_ttbar_nonallhad|PoPy8_Ztautau).*", "TruthTau_leading_Lxy", '3p_sm', 40e3, 1e6, 3, "TruthTau_leading_LeadingTrk_d0", -10, 10),
        (".*n09.*Stau.*", "TruthTau_leading_Lxy", '1p_n1', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_Lxy", '3p_n1', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 0, 2),
        (".*n09.*Stau.*", "TruthTau_leading_Lxy", '1p_stau', 40e3, 1e6, 1, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),
        (".*n09.*Stau.*", "TruthTau_leading_Lxy", '3p_stau', 40e3, 1e6, 3, "TruthTau_leading_LastSUSY_Mother_absPDGID", 1, 3),
    ]
    out = run(leadingPlotHist,argLIST2)

    print ','.join(out)
    num = find_max_number('.')+1
    os.system('hep_sub hist_sub.sh."%{ProcId}" -g atlas -n '+str(num))
