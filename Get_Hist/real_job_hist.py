import ROOT
import re
import pickle
import os
from doPlotHist import getProc, getName
import math
def getX(x, name):
    if "Lxy" in name:
        return  math.log(abs(x)+1e-10, 10)
    elif "d0" in name:
        return math.log(abs(x)+1e-10, 10)
    elif "Pt" in name:
        return x/1e3
    else:
        return x

def fill_hist(num, root, ifile, variable, ptcut, cut_branch, clow, chigh):
    procName = getProc(root)
    outname =getName(num, procName, variable, ptcut, cut_branch, clow, chigh)

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, weiArray = [],[]
    for event in file.analysis:
        ptpass = [ipt>ptcut for ipt in getattr(event, 'TruthTau_Vis_Pt')]
        if cut_branch!="no":
            passcut = [(getX(val,cut_branch)>clow and getX(val,cut_branch)<chigh) for val in getattr(event, cut_branch)]
            passcut = [(p and c) for p, c in zip(ptpass, passcut)]
        else:
            passcut = ptpass

        for var, goodtau in zip(getattr(event, variable), passcut):
            if goodtau:
                xArray.append(getX(var,variable))
                weiArray.append(getattr(event,'Weight'))
        # if 'Stau' not in root:
        #     if len(weiArray)>1e5: 
        #         break
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"wei":weiArray}, pick)
    return procName, outname

import argparse
parser = argparse.ArgumentParser(description='Calculate efficiency')
parser.add_argument('num', type=int, help='unique number for output file')
parser.add_argument('root', type=str, help='Root directory')
parser.add_argument('ifile', type=str, help='Input file')
parser.add_argument('truth_branch', type=str, help='Truth branch name')
parser.add_argument('ptcut', type=float, help='PT cut value')
parser.add_argument('cut_branch', type=str, help='Cut branch name')
# parser.add_argument('cfun_name', type=str, help='CFUN name')
parser.add_argument('clow', type=float, help='CLow value')
parser.add_argument('chigh', type=float, help='CHigh value')
args = parser.parse_args()

fill_hist(args.num, args.root, args.ifile, args.truth_branch, args.ptcut, args.cut_branch, args.clow, args.chigh)