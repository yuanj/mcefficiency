Working Dir:

Get_Hist: draw 1d hist

Get_Eff1: draw 1d eff

Get_Eff2: draw 2d eff and produce workspace

Backup for old scripts:

eff1.py: old eff script using pyroot

eff2.py: old eff script using pyroot

Ntuples -> /publicfs/atlas/atlasnew/SUSY/users/caiyc/RPV/EfficiencyStudy/xaodlearning/Ntuples/ # ntuples in yuchen folder

Ntuples2: ntuples in my folder

resol2.py: pT resolution

resol.py: pT resolution

PlotHist.py: draw distributions using pyROOT, very primitive.

uPlotHist.py: draw distributions using uproot

ePlotEff.py: draw efficiency using pyROOT

ePlotEffID.py: draw efficiency using pyROOT (id eff)

iPlotEff.py: draw efficiency using pyROOT with multiprocessing

uPlotEff.py: draw efficiency using uproot
