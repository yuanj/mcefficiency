import json
import numpy as np
import pickle
import ROOT
from array import array
from multiprocessing import Process

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+6, 
        ROOT.kYellow, ROOT.kRed
]
ROOT.gROOT.SetBatch(True)
def tobinning(x):
    return [len(x)-1,x]

def drawEff(jfile_list, bins):
    if not isinstance(jfile_list, list):
        jfile_list = [jfile_list]
    binning = tobinning(bins)

    jfile=jfile_list[0]
    PlotTitle = jfile_list[0].split('.')[0]
    if 'd0' in PlotTitle or 'Lxy' in PlotTitle:
        PlotTitle = 'log10_'+PlotTitle

    data = {}
    for jfile in jfile_list:
        with open(jfile,'r') as f:
            file_data = json.load(f)
            data.update(file_data)

    sortedName = sorted(list(data.keys()))
    canvas = ROOT.TCanvas("", "", 1600, 1200)
    isFirst = True
    legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)
    TTstore=[]

    for i, process in enumerate(sortedName):
        if process=="ttbar_allhad": continue
        branchList, effList, weiList = [],[],[]
        for filename in data[process]:
            with open(filename,"rb") as file:
                datadict = pickle.load(file)
                branchList.extend(datadict["xval"])
                effList.extend(datadict["eff"])
                weiList.extend(datadict["wei"])
        weiList = np.array(weiList)
        weiList = weiList / weiList.sum()
        arg_h1 = [process,process]+binning
        arg_h2 = [process+"down",process+"down"]+binning
        hist = ROOT.TH1F(*arg_h1)
        hist_down = ROOT.TH1F(*arg_h2)

        for x,e,w in zip(branchList, effList, weiList):
            hist.Fill(x,e*w)
            hist_down.Fill(x,w)

        hist.Divide(hist_down)
        hist.SetLineColor(UserColors[i])
        hist.SetLineWidth(3)
        if isFirst:
            hist.Draw()
            hist.SetXTitle(PlotTitle)
            hist.SetYTitle("Efficiency")
            hist.SetTitle('')
            hist.GetYaxis().SetRangeUser(0., 1.);
            isFirst = False
        else:
            hist.Draw('SAME')
        hist.SetStats(0)
        legend.AddEntry(hist, process, "l")
        TTstore.append(hist)
    legend.Draw()
    canvas.SaveAs("EFF_"+jfile_list[0].split('.')[0]+".png")

lin1 = array('f', [-3,0.5]+[x/2. for x in range(2,6,1)]+[4,])
lin2 = array('f', [-3,0]+[x/2. for x in range(1,4,1)]+[4,])
lin3 = array('f', [-3,1]+[x/2. for x in range(3,7,1)]+[4])
#din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
din1 = array('f', [-6,-0.5]+[x/2. for x in range(0,9,1)])
din2 = array('f', [-6,-0.5]+[x/2. for x in range(0,9,1)])
ptbin = array('f', list(range(40,210,10)))
#ein1 = array('f', [x/10. for x in range(-30,31,5)])
#ein2 = array('f', [-3,-1.52,-1.37,1.37,1.52,3])

arglist = [
    #['eff_TruthTau_leading_LeadingTrk_d0_l1.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_l2.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_m1.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_m2.json',din1],
    #[['eff_TruthTau_leading_LeadingTrk_d0_l1.json','eff_TruthTau_leading_LeadingTrk_d0_s1.json'],din1],
    #[['eff_TruthTau_leading_LeadingTrk_d0_l2.json','eff_TruthTau_leading_LeadingTrk_d0_s1.json'],din1],
    #[['eff_TruthTau_leading_LeadingTrk_d0_m1.json','eff_TruthTau_leading_LeadingTrk_d0_s1.json'],din1],
    #[['eff_TruthTau_leading_LeadingTrk_d0_m2.json','eff_TruthTau_leading_LeadingTrk_d0_s1.json'],din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_s1.json',din2],
    #['eff_TruthTau_leading_Lxy_l1.json',lin1],
    #['eff_TruthTau_leading_Lxy_l2.json',lin2],
    #['eff_TruthTau_leading_Lxy_m1.json',lin3],
    #['eff_TruthTau_leading_Lxy_m2.json',lin3],
    #['eff_TruthTau_leading_Lxy_s1.json',lin1],
    [['eff_TruthTau_leading_Pt_l1.json','eff_TruthTau_leading_Pt_s1.json'],ptbin],
    [['eff_TruthTau_leading_Pt_l2.json','eff_TruthTau_leading_Pt_s1.json'],ptbin],
    [['eff_TruthTau_leading_Pt_m1.json','eff_TruthTau_leading_Pt_s1.json'],ptbin],
    [['eff_TruthTau_leading_Pt_m2.json','eff_TruthTau_leading_Pt_s1.json'],ptbin],
    #['eff_TruthTau_leading_Pt_s1.json',ptbin],
]
processLst = []
for pargs in arglist:
    processLst.append(Process(target=drawEff, args=(pargs)))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
