import json
import numpy as np
import ROOT
import pickle
from array import array
from multiprocessing import Process

UserColors = [
        ROOT.kBlack, ROOT.kBlue, ROOT.kGreen, 
        ROOT.kRed, ROOT.kOrange, ROOT.kTeal, 
        ROOT.kViolet, ROOT.kPink+6, 
        ROOT.kYellow, ROOT.kRed
]
ROOT.gROOT.SetBatch(True)

def tobinning(x):
    return [len(x)-1,x]

def drawEff(jfile, bins):
    binning = tobinning(bins)
    PlotTitle = 'log10_'+jfile.split('.')[0] if 'd0' in jfile or 'Lxy' in jfile else jfile.split('.')[0]
    with open(jfile) as f:
        data = json.load(f)
        sortedName = sorted(list(data.keys()))

        canvas = ROOT.TCanvas("", "", 1600, 1200)
        isFirst = True
        legend = ROOT.TLegend(0.73, 0.65, 0.9, 0.9)
        TTstore=[]

        for i, process in enumerate(sortedName):
            if process=="ttbar_allhad": continue
            branchList, effList, weiList = [],[],[]
            for filename in data[process]:
                with open(filename,"rb") as file:
                    datadict = pickle.load(file)
                    branchList.extend(datadict["xval"])
                    effList.extend(datadict["eff"])
                    weiList.extend(datadict["wei"])
            weiList = np.array(weiList)
            weiList = weiList / weiList.sum()
            arg_h1 = [process,process]+binning
            arg_h2 = [process+"down",process+"down"]+binning
            hist = ROOT.TH1F(*arg_h1)
            hist_down = ROOT.TH1F(*arg_h2)

            for x,e,w in zip(branchList, effList, weiList):
                hist.Fill(x,e*w)
                hist_down.Fill(x,w)

            hist.Divide(hist_down)
            hist.SetLineColor(UserColors[i])
            hist.SetLineWidth(3)
            if isFirst:
                hist.Draw()
                hist.SetXTitle(PlotTitle)
                hist.SetYTitle("Efficiency")
                hist.SetTitle('')
                hist.GetYaxis().SetRangeUser(0., 1.);
                isFirst = False
            else:
                hist.Draw('SAME')
            hist.SetStats(0)
            legend.AddEntry(hist, process, "l")
            TTstore.append(hist)
        legend.Draw()
        canvas.SaveAs("EFF_"+jfile.split('.')[0]+".png")

lin1 = array('f', [-3,-0.5]+[x/2. for x in range(0,9,1)])
lin1 = array('f', [-3, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0])
lin1 = array('f', [-3, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 4.0])
din1 = array('f', [-6,-3]+[x/2. for x in range(-5,9,1)])
din2 = array('f', [-6,-1.5]+[x/2. for x in range(-2,9,1)])
ptbin = array('f', list(range(40,210,10)))
ein1 = array('f', [x/10. for x in range(-30,31,5)])
ein2 = array('f', [-3,-1.52,-1.37,1.37,1.52,3])

arglist = [
    #['eff_TruthTau_leading_Lxy_bl1.json',lin1],
    #['eff_TruthTau_leading_Lxy_bl3.json',lin1],
    #['eff_TruthTau_leading_Lxy_bl5.json',lin1],

    ['eff_TruthTau_leading_LeadingTrk_d0_sb1.json',din1],
    ['eff_TruthTau_leading_LeadingTrk_d0_sb3.json',din1],
    ['eff_TruthTau_leading_LeadingTrk_d0_sb5.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_sb0.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_a1.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_a2.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_a3.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_a4.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_a5.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_as.json',din1],

    #['eff_TruthTau_leading_LeadingTrk_d0_b1.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_b2.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_b3.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_b4.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_b5.json',din1],
    #['eff_TruthTau_leading_LeadingTrk_d0_bs.json',din1],

    #['eff_TruthTau_leading_Eta_1.json',ein2],
    #['eff_TruthTau_leading_Eta_2.json',ein2],
    #['eff_TruthTau_leading_Eta_3.json',ein2],
    #['eff_TruthTau_leading_Eta_4.json',ein2],
    #['eff_TruthTau_leading_Eta_5.json',ein2],
    #['eff_TruthTau_leading_Eta_s.json',ein2],

    #['eff_TruthTau_leading_Pt_a1.json',ptbin],
    #['eff_TruthTau_leading_Pt_a2.json',ptbin],
    #['eff_TruthTau_leading_Pt_a3.json',ptbin],
    #['eff_TruthTau_leading_Pt_a4.json',ptbin],
    #['eff_TruthTau_leading_Pt_a5.json',ptbin],
    #['eff_TruthTau_leading_Pt_as.json',ptbin],

    #['eff_TruthTau_leading_Pt_b1.json',ptbin],
    #['eff_TruthTau_leading_Pt_b2.json',ptbin],
    #['eff_TruthTau_leading_Pt_b3.json',ptbin],
    #['eff_TruthTau_leading_Pt_b4.json',ptbin],
    #['eff_TruthTau_leading_Pt_b5.json',ptbin],
    #['eff_TruthTau_leading_Pt_bs.json',ptbin],
]


processLst = []
for pargs in arglist:
    processLst.append(Process(target=drawEff, args=(pargs)))
    processLst[-1].start()
for i, proc in enumerate(processLst):
    proc.join()
