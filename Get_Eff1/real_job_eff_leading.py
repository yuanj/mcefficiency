import ROOT
import re
import pickle
import os
from doPlotEff import getProc,getName
import math

def getX(x, name):
    if "Lxy" in name:
        return  math.log(abs(x)+1e-10, 10)
    elif "d0" in name:
        return math.log(abs(x)+1e-10, 10)
    elif "Pt" in name:
        return x/1e3
    elif "Eta" in name:
        return abs(x)
    elif "absPDGID" in name:
        if x ==1000022:
            return 1
        elif x==1000015 or x==2000015:
            return 2
        else:
            return 0
    else:
        return x

def calc_eff(num, root, ifile, truth_branch, match_branch, ptcut, nprong, cut_branch, clow, chigh):
    procName = getProc(root)
    outname = getName(num, procName, truth_branch, match_branch, ptcut, nprong, cut_branch, clow, chigh)

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:
        ptpass = getattr(event, 'TruthTau_leading_Pt') > ptcut
        np_pass = (nprong<0) or (getattr(event, 'TruthTau_leading_ChargedPion') == nprong)
        etapass = abs(getattr(event, 'TruthTau_leading_Eta'))<1.37 or (abs(getattr(event, 'TruthTau_leading_Eta'))>1.52 and abs(getattr(event, 'TruthTau_leading_Eta'))<2.5)
        npbasepass = (getattr(event, 'TruthTau_leading_ChargedPion') == 1) or (getattr(event, 'TruthTau_leading_ChargedPion') == 3)
        if cut_branch!="no":
            val = getattr(event, cut_branch)
            passcut = (getX(val,cut_branch)>clow and getX(val,cut_branch)<chigh) and ptpass and np_pass and etapass and npbasepass
        else:
            passcut = ptpass and np_pass and etapass and npbasepass
        if passcut:
            xArray.append(getX(getattr(event, truth_branch),truth_branch))
            effArray.append(getattr(event, match_branch))
            weiArray.append(getattr(event,'Weight'))
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname

import argparse
parser = argparse.ArgumentParser(description='Calculate efficiency')
parser.add_argument('num', type=int, help='unique number for output file')
parser.add_argument('root', type=str, help='Root directory')
parser.add_argument('ifile', type=str, help='Input file')
parser.add_argument('truth_branch', type=str, help='Truth branch name')
parser.add_argument('match_branch', type=str, help='Match branch name')
parser.add_argument('ptcut', type=float, help='PT cut value')
parser.add_argument('nprong', type=float, help='nprong value')
parser.add_argument('cut_branch', type=str, help='Cut branch name')
parser.add_argument('clow', type=float, help='CLow value')
parser.add_argument('chigh', type=float, help='CHigh value')
args = parser.parse_args()

calc_eff(args.num, args.root, args.ifile, args.truth_branch, args.match_branch, args.ptcut, args.nprong, args.cut_branch, args.clow, args.chigh)
