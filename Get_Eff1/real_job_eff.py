import ROOT
import re
import pickle
import os
from doPlotEff import getProc,getName
import math
# def dsid_transfer(dsid):
#     if abs(dsid)==1000015 or abs(dsid)==2000015:
#         return 0 # stau
#     elif abs(dsid)==1000022:
#         return 1 #N1
#     elif abs(dsid)<1000: 
#         return 2
#     else:
#         return 3

def getX(x, name):
    if "Lxy" in name:
        return  math.log(abs(x)+1e-10, 10)
    elif "d0" in name:
        return math.log(abs(x)+1e-10, 10)
    elif "Pt" in name:
        return x/1e3
    else:
        return x

def calc_eff(num, root, ifile, truth_branch, match_branch, ptcut, cut_branch, clow, chigh):
    procName = getProc(root)
    outname = getName(num, procName, truth_branch, match_branch, ptcut, cut_branch, clow, chigh)

    file = ROOT.TFile.Open(os.path.join(root, ifile))
    xArray, effArray, weiArray = [],[],[]
    for event in file.analysis:
        ptpass = [ipt>ptcut for ipt in getattr(event, 'TruthTau_Vis_Pt')]
        if cut_branch!="no":
            passcut = [(getX(val,cut_branch)>clow) and (getX(val,cut_branch)<chigh) for val in getattr(event, cut_branch)]
            passcut = [(p and c) for p, c in zip(ptpass, passcut)]
        else:
            passcut = ptpass
        for var, ismatched, goodtau in zip(getattr(event, truth_branch), getattr(event, match_branch), passcut):
            if goodtau:
                xArray.append(getX(var,truth_branch))
                effArray.append(ismatched)
                weiArray.append(getattr(event,'Weight'))
        # if 'Stau' not in root:
        #     if len(weiArray)>1e5: 
        #         break                
    with open(outname, "wb") as pick:
        pickle.dump({"label":procName,"xval":xArray,"eff":effArray,"wei":weiArray}, pick)
    return procName, outname

import argparse
parser = argparse.ArgumentParser(description='Calculate efficiency')
parser.add_argument('num', type=int, help='unique number for output file')
parser.add_argument('root', type=str, help='Root directory')
parser.add_argument('ifile', type=str, help='Input file')
parser.add_argument('truth_branch', type=str, help='Truth branch name')
parser.add_argument('match_branch', type=str, help='Match branch name')
parser.add_argument('ptcut', type=float, help='PT cut value')
parser.add_argument('cut_branch', type=str, help='Cut branch name')
parser.add_argument('clow', type=float, help='CLow value')
parser.add_argument('chigh', type=float, help='CHigh value')
args = parser.parse_args()

calc_eff(args.num, args.root, args.ifile, args.truth_branch, args.match_branch, args.ptcut, args.cut_branch, args.clow, args.chigh)